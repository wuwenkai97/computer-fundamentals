**1.B+树内节点不存储数据，所有 data 存储在叶节点导致查询时间复杂度固定为 log n。而B-树查询时间复杂度不固定，与 key 在树中的位置有关，最好为O(1)。**

如下所示B-树/B+树查询节点 key 为 50 的 data。

B-树：

![image-20210724230551696](../../../picture/Theoretical/MySQL/B树.png)

B-树

从上图可以看出，key 为 50 的节点就在第一层，B-树只需要一次磁盘 IO 即可完成查找。所以说B-树的查询最好时间复杂度是 O(1)。

B+树：



![image-20210724230603008](../../../picture/Theoretical/MySQL/B+树非叶子节点.png)

B+树

**由于B+树所有的 data 域都在根节点，所以查询 key 为 50的节点必须从根节点索引到叶节点，时间复杂度固定为 O(log n)。**

> 点评：B树的由于每个节点都有key和data，所以查询的时候可能不需要O(logn)的复杂度，甚至最好的情况是O(1)就可以找到数据，而B+树由于只有叶子节点保存了data，所以必须经历O(logn)复杂度才能找到数据

**2. B+树叶节点两两相连可大大增加区间访问性，可使用在范围查询等，而B-树每个节点 key 和 data 在一起，则无法区间查找。**

![image-20210724230610724](../../../picture/Theoretical/MySQL/B+树叶子节点.png)

B+树



根据空间局部性原理：如果一个存储器的某个位置被访问，那么将它附近的位置也会被访问。

B+树可以很好的利用局部性原理，若我们访问节点 key为 50，则 key 为 55、60、62 的节点将来也可能被访问，**我们可以利用磁盘预读原理提前将这些数据读入内存，减少了磁盘 IO 的次数。**
 **当然B+树也能够很好的完成范围查询。比如查询 key 值在 50-70 之间的节点。**

> 点评：由于B+树的叶子节点的数据都是使用链表连接起来的，而且他们在磁盘里是顺序存储的，所以当读到某个值的时候，磁盘预读原理就会提前把这些数据都读进内存，使得范围查询和排序都很快

**3.B+树更适合外部存储。由于内节点无 data 域，每个节点能索引的范围更大更精确**

这个很好理解，由于B-树节点内部每个 key 都带着 data 域，而B+树节点只存储 key 的副本，真实的 key 和 data 域都在叶子节点存储。前面说过磁盘是分 block 的，一次磁盘 IO 会读取若干个 block，具体和操作系统有关，**那么由于磁盘 IO 数据大小是固定的，在一次 IO 中，单个元素越小，量就越大**。**这就意味着B+树单次磁盘 IO 的信息量大于B-树**，从这点来看B+树相对B-树磁盘 IO 次数少。

> 点评：由于B树的节点都存了key和data，而B+树只有叶子节点存data，非叶子节点都只是索引值，没有实际的数据，这就时B+树在一次IO里面，能读出的索引值更多。从而减少查询时候需要的IO次数！

![image-20210724230618744](../../../picture/Theoretical/MySQL/B家属和B树对比.png)

B/B+树

从上图可以看出相同大小的区域，B-树仅有 2 个 key，而B+树有 3 个 key。