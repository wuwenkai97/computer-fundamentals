# DDL(Data Definition Language)

查看所有数据库名称：SHOW DATABASES；
切换数据库：USE mydb1;
创建数据库：CREATE DATABASE [IF NOT EXISTS] mydb1；
删除数据库：DROP DATABASE [IF EXISTS] mydb1；
修改数据库编码：ALTER DATABASE mydb1 CHARACTER SET utf8

```mysql
# 创建表
CREATE TABLE stu(
    sid     CHAR(6),
    sname   VARCHAR(20),
    age     INT,
    gender  VARCHAR(10) 
);

CREATE TABLE emp(
    eid     CHAR(6),
    ename   VARCHAR(50),
    age     INT,
    gender  VARCHAR(6),
    birthday    DATE,
    hiredate    DATE,
    salary  DECIMAL(7,2),
    resume  VARCHAR(1000)
);
```

# DML(Data Manipulation Language)

```mysql
# 插入数据
INSERT INTO stu(sid, sname) VALUES('s_1001', 'zhangSan');
INSERT INTO stu VALUES('s_1002', 'liSi', 32, 'female');
# 修改数据
UPDATE stu SET sname=’liSi’, age=’20’ WHERE age>50 AND gender=’male’;
# 删除数据
DELETE FROM stu WHERE sname=’chenQi’ OR age > 30;
DELETE FROM stu; 
# truncate 是先DROP TABLE，再CREATE TABLE。而且TRUNCATE删除的记录是无  法回滚的，但DELETE删除的记录是可以回滚的
TRUNCATE TABLE stu;
```

# DCL(Data Control Language)

```mysql
# 创建用户: CREATE USER 用户名@地址 IDENTIFIED BY '密码';
CREATE USER user1@localhost IDENTIFIED BY ‘123’; 
CREATE USER user2@’%’ IDENTIFIED BY ‘123’; 
# 给用户授权: GRANT 权限1, … , 权限n ON 数据库.* TO 用户名
GRANT CREATE,ALTER,DROP,INSERT,UPDATE,DELETE,SELECT ON mydb1.* TO user1@localhost;
GRANT ALL ON mydb1.* TO user2@localhost;
# 撤销授权: REVOKE权限1, … , 权限n ON 数据库.* FORM 用户名
REVOKE CREATE,ALTER,DROP ON mydb1.* FROM user1@localhost;
# 查看用户权限:SHOW GRANTS FOR 用户名
SHOW GRANTS FOR user1@localhost;
# 删除用户:DROP USER 用户名
DROP USER user1@localhost;
# 修改用户密码
USE mysql;
UPDATE USER SET PASSWORD=PASSWORD(‘密码’) WHERE User=’用户名’ and Host=’IP’;
FLUSH PRIVILEGES;
#------------------
UPDATE USER SET PASSWORD=PASSWORD('1234') WHERE User='user2' and Host=’localhost’;
FLUSH PRIVILEGES;
```

# DQL(Data Query Language)

```mysql
SELECT selection_list /*要查询的列名称*/
  FROM table_list /*要查询的表名称*/
  WHERE condition /*行条件*/
  GROUP BY grouping_columns /*对结果分组*/
  HAVING condition /*分组后的行条件*/
  ORDER BY sorting_columns /*对结果分组*/
  LIMIT offset_start, row_count /*结果限定*/
```

## 条件查询

- =、!=、<>、<、<=、>、>=；
- BETWEEN…AND；
- IN(set)；
- IS NULL；
- AND；
- OR；
- NOT；

## 模糊查询

- “*_”:匹配任意一个字母，5个“*”表示5个任意字母
- “%”:匹配0~n个任何字母 “

## 字段控制查询

- 去除重复记录 :distinct

  `SELECT DISTINCT sal FROM emp;`

- 给列名添加别名

  `SELECT *, sal+IFNULL(comm,0) AS total FROM emp;`

## 聚合函数

1. COUNT()：统计指定列不为NULL的记录行数；
2. MAX()：计算指定列的最大值，是字符串类型，那么使用字符串排序运算；
3. MIN()：计算指定列的最小值，是字符串类型，那么使用字符串排序运算；
4. SUM()：计算指定列的数值和，不是数值类型，计算结果为0；
5. AVG()：计算指定列的平均值，不是数值类型，那么计算结果为0；

## LIMIT

limit 起始行 , 查询行数 //起始行从0开始，为开区间

# 完整性约束

* 主键 ：primary key
  1. 创建表：定义列时指定主键
  2. 创建表：定义列之后独立指定主
  3. 修改表时指定主键`ALTER TABLE stu ADD PRIMARY KEY(sid);`
  4. 删除主键`ALTER TABLE stu DROP PRIMARY KEY;`

* 主键自增长 ：auto_increment（主键必须是整型才可以自增长）

  1. 创建表时设置主键自增长

     ```mysql
     CREATE TABLE stu(
         sid INT PRIMARY KEY AUTO_INCREMENT,
         sname   VARCHAR(20),
         age     INT,
         gender  VARCHAR(10)
     );
     ```

  2. 修改表时设置主键自增长`ALTER TABLE stu CHANGE sid sid INT AUTO_INCREMENT;`

  3. 修改表时删除主键自增长`ALTER TABLE stu CHANGE sid sid INT;`

* 非空：NOT NULL

  字段设为非空后，插入记录时必须给值

* 唯一：UNIQUE

  字段指定唯一约束后，字段的值必须是唯一的

* 外键

  **外键是另一张表的主键**；外键就是用来约束这一列的值必须是另一张表的主键值

