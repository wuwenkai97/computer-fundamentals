**Servlet是用来处理客户端请求的动态资源，也就是当我们在浏览器中键入一个地址回车跳转后，请求就会被发送到对应的Servlet上进行处理。**

**Servlet的任务有：**

* 接收请求数据：我们都知道客户端请求会被封装成HttpServletRequest对象，里面包含了请求头、参数等各种信息。
* 处理请求：通常我们会在service、doPost或者doGet方法进行接收参数，并且调用业务层（service）的方法来处理请求。
* 完成响应：处理完请求后，我们一般会转发（forward）或者重定向（redirect）到某个页面，转发是HttpServletRequest中的方法，重定向是HttpServletResponse中的方法，两者是有很大区别的。

* Servlet的创建：Servlet可以在第一次接收请求时被创建，也可以在在服务器启动时就被创建，这需要在web.xml的< servlet>中添加一条配置信息`<load-on-startup>5</load-on-startup>`，当值为0或者大于0时，表示容器在应用启动时就加载这个servlet，当是一个负数时或者没有指定时，则指示容器在该servlet被请求时才加载。

**Servlet的生命周期方法：**

> void init(ServletConfig)
> servlet的初始化方法，只在创建servlet实例时候调用一次，Servlet是单例的，整个服务器就只创建一个同类型Servlet

> void service(ServletRequest,ServletResponse)
> servlet的处理请求方法，在servle被请求时，会被马上调用，每处理一次请求，就会被调用一次。ServletRequest类为请求类，ServletResponse类为响应类

> void destory()
> servlet销毁之前执行的方法，只执行一次，用于释放servlet占有的资源，通常Servlet是没什么可要释放的，所以该方法一般都是空的

**Servlet的其他重要方法：**

> ServletConfig getServletConfig()
> 获取servlet的配置信息的方法，所谓的配置信息就是WEB-INF目录下的web.xml中的servlet标签里面的信息

> String getServletInfo()
> 获取servlet的信息方法

Servlet的配置：

```xml
<servlet>
  <servlet-name>LoginServlet</servlet-name>
  <servlet-class>com.briup.estore.web.servlet.LoginServlet</servlet-class>
</servlet>
<servlet-mapping>
  <servlet-name>LoginServlet</servlet-name>
  <url-pattern>/login</url-pattern>
</servlet-mapping>
```

**继承HttpServlet的用法：**

```java
public class ServletDemo3 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("haha");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("ee");
        doGet(req,resp);
    }

}
```

