# AOP底层
Spring默认为动态代理，SpringBoot默认为Cglib

# AOP使用方式

## AOP术语

在讲解AOP术语之前，我们先来看一下下面这两张图，它们就是第三部分案例需求的扩展(针对这些扩展的需求，我们只进行分析，在此基础上去进一步回顾AOP，不进行实现)

![img](../../../picture/Theoretical/Spring/AOP思想设计.png)

 

上图描述的就是未采用AOP思想设计的程序，当我们红色框中圈定的方法时，会带来大量的重复劳动。程序中充斥着大量的重复代码，使我们程序的独立性很差。而下图中是采用了AOP思想设计的程序，它把红框部分的代码抽取出来的同时，运用动态代理技术，在运行期对需要使用的业务逻辑方法进行增强。

![img](../../../picture/Theoretical/Spring/AOP实现.png)

 

**1.2 AOP** 术语

![img](../../../picture/Theoretical/Spring/AOP术语.png)

连接点：方法开始时、结束时、正常运行完毕时、方法异常时等这些特殊的时机点，我们称之为连接点,项目中每个方法都有连接点,连接点是一种候选点

切入点：指定AOP思想想要影响的具体方法是哪些，描述感兴趣的方法

**Advice增强：**

第一个层次：指的是横切逻辑

第二个层次︰方位点（在某一些连接点上加入横切逻辑，那么这些连接点就叫做方位点，描述的是具体的特殊时机)

**Aspect切面：**切面概念是对上述概念的一个综合

Aspect切面=切入点+增强=切入点（锁定方法)＋方位点（锁定方法中的特殊时机)＋横切逻辑

**众多的概念，目的就是为了锁定要在哪个地方插入什么横切逻辑代码**

## 切面类

1. 必要的依赖

	```xml
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-aop</artifactId>
	</dependency>
	```

2. 日志类和Service

   ```java
   
   package com.space.aspect.bo;
    
   import lombok.Data;
    
   /**
    * 系统日志bo
    * @author zhuzhe
    * @date 2018/6/4 9:36
    * @email 1529949535@qq.com
    */
   @Data
   public class SysLogBO {
    
       private String className;
    
       private String methodName;
    
       private String params;
    
       private Long exeuTime;
    
       private String remark;
    
       private String createDate;
   ```

   ```java
   package com.space.aspect.service;
    
   import com.space.aspect.bo.SysLogBO;
   import lombok.extern.slf4j.Slf4j;
   import org.springframework.stereotype.Service;
    
   /**
    * @author zhuzhe
    * @date 2018/6/4 9:41
    * @email 1529949535@qq.com
    */
   @Slf4j
   @Service
   public class SysLogService {
    
       public boolean save(SysLogBO sysLogBO){
           // 这里就不做具体实现了
           log.info(sysLogBO.getParams());
           return true;
       }
   }
   ```

3. 定义一个日志注解

   ```java
   package com.space.aspect.anno;
    
   import java.lang.annotation.*;
    
   /**
    * 定义系统日志注解
    * @author zhuzhe
    * @date 2018/6/4 9:24
    * @email 1529949535@qq.com
    */
   @Target(ElementType.METHOD)
   @Retention(RetentionPolicy.RUNTIME)
   @Documented
   public @interface SysLog {
       String value() default "";
   }
   ```

4. 定义切面类

   ```java
   package com.space.aspect.aspect;
    
   import com.google.gson.Gson;
   import com.space.aspect.anno.SysLog;
   import com.space.aspect.bo.SysLogBO;
   import com.space.aspect.service.SysLogService;
   import org.aspectj.lang.ProceedingJoinPoint;
   import org.aspectj.lang.annotation.Around;
   import org.aspectj.lang.annotation.Aspect;
   import org.aspectj.lang.annotation.Pointcut;
   import org.aspectj.lang.reflect.MethodSignature;
   import org.springframework.beans.factory.annotation.Autowired;
   import org.springframework.stereotype.Component;
    
   import java.lang.reflect.Method;
   import java.text.SimpleDateFormat;
   import java.util.ArrayList;
   import java.util.Date;
   import java.util.List;
    
   /**
    * 系统日志切面
    * @author zhuzhe
    * @date 2018/6/4 9:27
    * @email 1529949535@qq.com
    */
   @Aspect  // 使用@Aspect注解声明一个切面
   @Component
   public class SysLogAspect {
    
       @Autowired
       private SysLogService sysLogService;
    
       /**
        * 这里我们使用注解的形式
        * 当然，我们也可以通过切点表达式直接指定需要拦截的package,需要拦截的class 以及 method
        * 切点表达式:   execution(...)
        */
       @Pointcut("@annotation(com.space.aspect.anno.SysLog)")
       public void logPointCut() {}
    
       /**
        * 环绕通知 @Around  ， 当然也可以使用 @Before (前置通知)  @After (后置通知)
        * @param point
        * @return
        * @throws Throwable
        */
       @Around("logPointCut()")
       public Object around(ProceedingJoinPoint point) throws Throwable {
           long beginTime = System.currentTimeMillis();
           Object result = point.proceed();
           long time = System.currentTimeMillis() - beginTime;
           try {
               saveLog(point, time);
           } catch (Exception e) {
           }
           return result;
       }
    
       /**
        * 保存日志
        * @param joinPoint
        * @param time
        */
       private void saveLog(ProceedingJoinPoint joinPoint, long time) {
           MethodSignature signature = (MethodSignature) joinPoint.getSignature();
           Method method = signature.getMethod();
           SysLogBO sysLogBO = new SysLogBO();
           sysLogBO.setExeuTime(time);
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
           sysLogBO.setCreateDate(dateFormat.format(new Date()));
           SysLog sysLog = method.getAnnotation(SysLog.class);
           if(sysLog != null){
               //注解上的描述
               sysLogBO.setRemark(sysLog.value());
           }
           //请求的 类名、方法名
           String className = joinPoint.getTarget().getClass().getName();
           String methodName = signature.getName();
           sysLogBO.setClassName(className);
           sysLogBO.setMethodName(methodName);
           //请求的参数
           Object[] args = joinPoint.getArgs();
           try{
               List<String> list = new ArrayList<String>();
               for (Object o : args) {
                   list.add(new Gson().toJson(o));
               }
               sysLogBO.setParams(list.toString());
           }catch (Exception e){ }
           sysLogService.save(sysLogBO);
       }
   }
   ```

