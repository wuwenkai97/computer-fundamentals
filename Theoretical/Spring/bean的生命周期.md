1. Spring对bean进行实例化，调用bean的构造参数

2. 设置对象属性，调用bean的set方法，将属性注入到bean的属性中

3. 检查bean是否实现BeanNameAware、BeanFactoryAware、ApplicationContextAware接口，如果实现了这几个接口Spring会分别调用其中实现的方法。

   BeanNameAware：setBeanName(String name)方法，参数是bean的ID

   BeanFactoryAware：setBeanFactory(BeanFactory bf)方法，参数是BeanFactory容器

   ApplicationContextAware：setApplicationContext(ApplicationContext context)方法，参数是bean所在的引用的上下文，**如果是**

   **用Bean工厂创建bean，那就可以忽略ApplicationContextAware**。

4. 如果bean是否实现BeanPostProcessor接口，Spring会在初始化方法的前后分别调用postProcessBeforeInitialization和postProcessAfterInitialization方法

5. 如果bean是否实现InitalizingBean接口，将调用afterPropertiesSet()方法

6. 如果bean声明初始化方法，也会被调用

7. 使用bean，bean将会一直保留在应用的上下文中，直到该应用上下文被销毁。

8. 检查bean是否实现DisposableBean接口，Spring会调用它们的destory方法

9. 如果bean声明销毁方法，该方法也会被调用

