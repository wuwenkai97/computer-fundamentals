package com.theoretical.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @Author WWK wuwenkai97@163.com
 * @Date 2022/5/8 18:01
 * @Description TODO
 **/
public class ComparableTest {
    public static void main(String[] args) {
        Integer[] array = {1, 3, 6, 2, 7, 10, 8};
        Arrays.sort(array, new Comparator<Integer>() {
            public int compare(Integer a, Integer b) {
                return b-a;
            }
        });
        System.out.println(Arrays.toString(array));
    }
}
