package com.theoretical.base;

import java.util.ArrayList;
import java.util.List;

public class PassedByValue {

    public static void main(String[] args) {
//        int a = 1;
//        test1(a);
//        System.out.println(a);

        List<Integer> arrays1 = new ArrayList<>();
        test2(arrays1);
        System.out.println(arrays1);

        String s = "b";
        test3(s);
        System.out.println(s);
    }

//    public static void test1(int a) {
//        a++;
//        System.out.println(a);
//    }

    public static void test2(List<Integer> arrays1) {
        arrays1.add(1);
        System.out.println(arrays1);
    }

    public static void test3(String s) {
        s += "a";
        System.out.println(s);
    }
}
