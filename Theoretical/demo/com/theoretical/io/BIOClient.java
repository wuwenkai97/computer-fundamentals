package com.theoretical.io;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class BIOClient {

    public static void main(String[] args) {
        OutputStream outputStream = null;
        try {
            Socket socket = new Socket("127.0.0.1", 3333);

            outputStream = socket.getOutputStream();
            outputStream.write("Hello, I’m Client \n".getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
