package com.theoretical.io;


import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class BIOServer {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(3333);
            while (true) {
                Socket socket = serverSocket.accept();
                new Thread(() -> {
                    InputStream inputStream = null;
                    try {
                        inputStream = socket.getInputStream();
                        int len;
                        byte[] buffer = new byte[1024];
                        while ((len = inputStream.read(buffer)) != -1) {
                            System.out.print(new String(buffer, 0, len));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                ).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
