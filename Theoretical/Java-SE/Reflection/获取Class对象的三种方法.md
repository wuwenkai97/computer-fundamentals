```java
@Test
public void test3() throws ClassNotFoundException {
    //前三种方式必须掌握
    //方式一：调用运行时类的属性
    Class clazz1 = Person.class;
    System.out.println(clazz1);

    //方式二：调用运行时类的对象,调用getClass()
    Person p1 = new Person();
    Class clazz2 = p1.getClass();
    System.out.println(clazz2);

    //方式三：调用Class的静态方法：forName(String classPath)
    Class clazz3 = Class.forName("com.company.Person");
    System.out.println(clazz3);

    System.out.println(clazz1 == clazz2);//true
    System.out.println(clazz1 == clazz3);//true
    System.out.println(clazz2 == clazz3);//true

    //方式四：使用类的加载器，ClassLoader
    ClassLoader classLoader = ReflectTest.class.getClassLoader();
    Class clazz4 = classLoader.loadClass("com.company.Person");
    System.out.println(clazz4);
}
```

