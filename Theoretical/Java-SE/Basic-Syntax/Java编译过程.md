javac是一种编译器，将java源代码编译成JVM可以识别的二进制码（字节码），就是将 *.java文件转换为 *.class文件

javac的结构主要分为：词法分析器，语法分析器，语义分析器，代码生成器

![在这里插入图片描述](../../../../picture/Theoretical/Java-SE/Basic-Syntax/javac.png)

1. **词法分析器：**逐行读取源代码，将源代码匹配到Token流，即识别每一个单词是什么东西，比如package匹配到Token.PACKAGE，class匹配到Token.CLASS，类名或者变量名匹配成Token.IDENTIFIER。
2. **语法分析器：**主要将Token流转换为更加结构化的语法树，就好像将上边的单词组装成一个完整的句子。
3. **语义分析器：**语法树再精简和处理，如添加默认构造函数，检查变量初始化，类型是否匹配，检查exception是否抛出等等
4. **代码生成器：**将语法树生成java字节码，可以理解为中间语言。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191109102646178.png)

**上图就是Java文件从编码完成到最终执行的整个过程，编译器就是上文提到的javac，而`.class`文件是在JVM中运行的，由JVM转换成机器码后，计算机再处理机器码，实现程序的运行。**

