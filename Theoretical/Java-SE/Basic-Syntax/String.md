# new String()和直接赋值的差别

`String str1 = new String("a")`和`String str1 = "a"`的差别如下：

1. 前者会在堆中创建一个String对象，然后区常量池寻找是否含有“a“，若不存在，则在常量池创建一个String
2. 后者会先去常量池查看是否有字符串”a“，如果有，那么直接返回该常量的引用；否则，创建常量，然后返回引用

# String、StringBuffer和StringBuilder的区别

String是不可变对象，后两者是可变对象，其中StringBuffer是线程安全的，StringBuilder是线程不安全的

