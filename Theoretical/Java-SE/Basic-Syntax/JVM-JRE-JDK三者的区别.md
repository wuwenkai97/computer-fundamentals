## JDK、JRE及JVM的定义

* JVM

  **Java Virtual Machine**是Java虚拟机，Java程序要运行在虚拟机上，不同的平台有自己的虚拟机，因此Java语言可以跨平台

* JRE

  **Java Runtime Environment**包括Java虚拟机和Java程序所需的核心类库等。核心类库主要是java.lang包：包含了运行Java程序必不可少的系统类，如基本数据类型、基本数学函数、字符串处理、线程、异常处理类等等，若仅仅想要运行一个Java程序，只需要在计算机中安装JRE即可

* JDK

  **Java Development Kit**是提供给Java开发人员使用的，其中包含了Java的开发工具（如编译工具javac.exe，打包工具jar.exe），也包括了JRE。所以，安装了JDK就无需再单独安装JRE了。

![image-20210626113733216](../../../../picture/Theoretical/Java-SE/Basic-Syntax/jdk构成.png)

