# JVM简介

<img src="../../../../picture/Theoretical/Java-SE/JVM/JVM结构.png" alt="image-20210628175610891" style="zoom: 67%;" />

JVM包含两个子系统和两个组件，两个子系统为Class loader(类装载)、Execution engine(执行引擎)；两个组件为Runtime data area(运行时数据区)、Native Interface(本地接口)。

- Class loader(类装载)：根据给定的全限定名类名(如：java.lang.Object)来装载class文件到Runtime data area中的method area。
- Execution engine（执行引擎）：执行classes中的指令。
- Native Interface(本地接口)：与native libraries交互，是其它编程语言交互的接口。
- Runtime data area(运行时数据区域)：这就是我们常说的JVM的内存。

<img src="../../../../picture/Theoretical/Java-SE/JVM/运行时数据区域结构.png" alt="img" style="zoom:50%;" />

不同虚拟机的运行时数据区可能略微有所不同，但都会遵从 Java 虚拟机规范， Java 虚拟机规范规定的区域分为以下 5 个部分：

- 程序计数器（Program Counter Register）：当前线程所执行的字节码的行号指示器，字节码解析器的工作是通过改变这个计数器的值，来选取下一条需要执行的字节码指令，分支、循环、跳转、异常处理、线程恢复等基础功能，都需要依赖这个计数器来完成；
- Java 虚拟机栈（Java Virtual Machine Stacks）：用于存储局部变量表、操作数栈、动态链接、方法出口等信息；
- 本地方法栈（Native Method Stack）：与虚拟机栈的作用是一样的，只不过虚拟机栈是服务 Java 方法的，而本地方法栈是为虚拟机调用 Native 方法服务的；
- Java 堆（Java Heap）：Java 虚拟机中内存最大的一块，是被所有线程共享的，几乎所有的对象实例都在这里分配内存；
- 方法区（Methed Area）：用于存储已被虚拟机加载的类信息、常量、静态变量、即时编译后的代码等数据。

# JVM三大性能调优参数-Xms、-Xmx、-Xss的含义

在使用java指令使可以将三个参数传入来运行jar包`java -Xms128m -Xmx128m -Xss256k -jar xxx.jar`

* -Xss：规定了每个线程虚拟机栈的大小，此配置影响了并发线程数的大小
* -Xms：初始Java堆的大小
* -Xmx：堆能达到的最大值

一般后两者设置成相同的值，因为如果不同的话，堆扩容会造成内存抖动影响程序稳定性