synchronized和ReetrantLock的区别

* ReentranLock
  * 基于AQS实现，位于java.util.concurrent.locks包下
  * 能够实现更细粒度的控制，如设置公平性：`ReentrantLock lock = new ReentrantLock(true)`，true表示倾向于将锁赋予等待时间最久的线程
  * 调用lock()后，必须调用unlock()解锁

* 公平锁和非公平锁：公平锁获取锁的顺序按先后调用lock方法的顺序；非公平锁抢占的顺序不一定，看运气

* 总结：
  * synchronized是关键字，ReentrantLock是类
  * ReentrantLock可以对获取锁的等待时间进行设置，避免死锁
  * ReentrantLock可以获取各种锁的信息
  * ReentrantLock可以灵活地实现多路通知
  * 机制：sync操作Mark Word，lock调用Unsafe类的park()方法

