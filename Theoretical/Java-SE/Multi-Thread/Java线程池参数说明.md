# 线程池

七大参数：

* corePoolSize 线程池核心线程大小：线程池中会维护一个最小的线程数量，即使这些线程处于空闲状态，他们也不会被销毁，除非设置了allowCoreThreadTimeOut。这里的最小线程数量即是corePoolSize。
* maximumPoolSize 线程池最大线程数量：一个任务被提交到线程池以后，首先会找有没有空闲存活线程，如果有则直接将任务交给这个空闲线程来执行，如果没有则会缓存到工作队列（后面会介绍）中，如果工作队列满了，才会创建一个新线程，然后从工作队列的头部取出一个任务交由新线程来处理，而将刚提交的任务放入工作队列尾部。线程池不会无限制的去创建新线程，它会有一个最大线程数量的限制，这个数量即由maximunPoolSize指定。
* keepAliveTime 空闲线程存活时间：一个线程如果处于空闲状态，并且当前的线程数量大于corePoolSize，那么在指定时间后，这个空闲线程会被销毁，这里的指定时间由keepAliveTime来设定
* unit 空闲线程存活时间单位：keepAliveTime的计量单位
* workQueue 工作队列：新任务被提交后，会先进入到此工作队列中，任务调度时再从队列中取出任务。jdk中提供了四种工作队列：
  * ArrayBlockingQueue：基于数组的有界阻塞队列，按FIFO排序。新任务进来后，会放到该队列的队尾，有界的数组可以防止资源耗尽问题。当线程池中线程数量达到corePoolSize后，再有新任务进来，则会将任务放入该队列的队尾，等待被调度。如果队列已经是满的，则创建一个新线程，如果线程数量已经达到maxPoolSize，则会执行拒绝策略。
  * LinkedBlockingQuene：基于链表的无界阻塞队列（其实最大容量为Interger.MAX），按照FIFO排序。由于该队列的近似无界性，当线程池中线程数量达到corePoolSize后，再有新任务进来，会一直存入该队列，而不会去创建新线程直到maxPoolSize，因此使用该工作队列时，参数maxPoolSize其实是不起作用的
  * SynchronousQuene：一个不缓存任务的阻塞队列，生产者放入一个任务必须等到消费者取出这个任务。也就是说新任务进来时，不会缓存，而是直接被调度执行该任务，如果没有可用线程，则创建新线程，如果线程数量达到maxPoolSize，则执行拒绝策略。
  * PriorityBlockingQueue：具有优先级的无界阻塞队列，优先级通过参数Comparator实现。
* threadFactory 线程工厂：创建一个新线程时使用的工厂，可以用来设定线程名、是否为daemon线程等等
* handler 拒绝策略：当工作队列中的任务已到达最大限制，并且线程池中的线程数量也达到最大限制，这时如果有新任务提交进来，该如何处理呢。这里的拒绝策略，就是解决这个问题的，jdk中提供了4中拒绝策略：
  * CallerRunsPolicy：该策略下，在调用者线程中直接执行被拒绝任务的run方法，除非线程池已经shutdown，则直接抛弃任务。
  * AbortPolicy：该策略下，直接丢弃任务，并抛出RejectedExecutionException异常。
  * DiscardPolicy：该策略下，直接丢弃任务，什么都不做。
  * DiscardOldestPolicy：该策略下，抛弃进入队列最早的那个任务，然后尝试把这次拒绝的任务放入队列

# 池化技术（扩展）

池化技术：把一些能够复用的东西（比如说数据库连接、线程）放到池中，避免重复创建、销毁的开销，从而极大提高性能。

在开发过程中我们会用到很多的连接池，像是数据库连接池、HTTP 连接池、Redis 连接池等等。而连接池的管理是连接池设计的核心，我就以数据库连接池为例，来说明一下连接池管理的关键点。

* 线程池

  1. 线程池的原理很简单，类似于操作系统中的缓冲区的概念，它的流程：先启动若干数量的线程，并让这些线程都处于睡眠状态，当客户端有一个新请求时，就会唤醒线程池中的某一个睡眠线程，让它来处理客户端的这个请求，当处理完这个请求后，线程又处于睡眠状态。

  2. 为什么要预先创建若干线程，而不是在需要的时候再创建？

     答：因为在数据量很大的条件下，某一时刻可能有大量的（上百个）并发请求，而线程创建的过程是比较耗时的，若此时对每个请求都新创建一个线程，那么会耗费大量的时间，造成拥塞。

  ![在这里插入图片描述](../../../../picture/Theoretical/Java-SE/Java-Util-Concurrent/线程池线程创建流程.png)

* 连接池

  1. 常见的数据库oracle、SQL server都有连接池技术，数据库连接池是在数据库启动时建立足够的数据库连接，并将这些连接组成一个连接池（简单说：在一个“池”里放了好多半成品的数据库联接对象），由应用程序动态地对池中的连接进行申请、使用和释放。对于多于连接池数据库连接数的并发请求，则在请求队列中排队等待。并且应用程序可以根据池中连接的使用率，动态增加或减少池中的连接数，这个增加减少由数据库连接池管理线程进行操作。

  2. 为什么要创建连接池？

     答：**因为需要连接数据库时再创建连接，然后用完就释放的方式会造成很多重复的数据库连接释放操作，且容易因为忘记释放而长期占用链接资源的缺陷。**而用数据库连接池负责分配、管理和释放数据库连接，它允许应用程序重复使用一个现有的数据库连接，而不是再重新建立一个；释放空闲时间超过最大空闲时间的数据库连接来避免因为没有释放数据库连接而引起的数据库连接遗漏。这项技术能明显提高对数据库操作的性能。

* 内存池

  1. 内存池是一种内存分配方式。通常我们习惯直接使用new、malloc等API申请分配内存，这样做的缺点在于：由于所申请内存块的大小不定，当频繁使用时会造成大量的内存碎片并进而降低性能。

  2. 一个解决方法是内存池：在启动的时候，一个内存池(Memory Pool)分配一块很大的内存，并将会将这个大块分成较小的块。每次你从内存池申请内存空间时，它会从先前已经分配的块中得到，而不是从操作系统。最大的优势在于：
     * 非常少(几没有) 堆碎片；
     * 比通常的内存申请/释放(比如通过malloc, new等)的方式快。