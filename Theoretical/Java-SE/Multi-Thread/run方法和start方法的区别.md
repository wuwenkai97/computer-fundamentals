run方法相当于执行普通线程类的方法，即在main线程执行逻辑；而start方法则会新起一个子线程，在子线程中执行逻辑

```java
// run
public class ThreadTest {
    private static void attack() {
        System.out.println("Fight");
        System.out.println("Current Thread is :" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread t = new Thread() {
            @Override
            public void run() {
                attack();
            }
        };
        System.out.println("current main thread is : " + Thread.currentThread().getName());
        t.run();
    }
}

/**
* 运行结果
* current main thread is : main
* Fight
* Current Thread is :main
*/
```
```java
// start
public class ThreadTest {
    private static void attack() {
        System.out.println("Fight");
        System.out.println("Current Thread is :" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread t = new Thread() {
            @Override
            public void run() {
                attack();
            }
        };
        System.out.println("current main thread is : " + Thread.currentThread().getName());
        t.start();
    }
}

/**
* 运行结果
* current main thread is : main
* Fight
* Current Thread is :Thread-0
*/
```

