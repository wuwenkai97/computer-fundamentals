1. 进入java安装的位置，输入Jconsole，然后弹出界面（或者进入安装目录/java/jdk1.70_80/bin/，点击Jconsole.exe）：

   <img src="C:/Users/Personal Computer/AppData/Roaming/Typora/typora-user-images/image-20210710180318003.png" alt="image-20210710180318003" style="zoom: 80%;" />

2. 然后点击连接进入界面并点击线程：

   <img src="C:/Users/Personal Computer/AppData/Roaming/Typora/typora-user-images/image-20210710180349210.png" alt="image-20210710180349210" style="zoom:80%;" />

3. 然后点击检测死锁，可以看到造成死锁的两个线程，以及死锁原因：

   ![image-20210710180543738](C:/Users/Personal Computer/AppData/Roaming/Typora/typora-user-images/image-20210710180543738.png)


   Thread-0：持有java.lang.Class@1694ce18，需要java.lang.Class@1feb0edd，但是java.lang.Class@1feb0edd却被Thread-1持有，然后陷入等待。


   Thread-1：持有java.lang.Class@1feb0edd，需要java.lang.Class@1694ce18，但是java.lang.Class@1694ce18却被Thread-0持有，然后陷入等待。