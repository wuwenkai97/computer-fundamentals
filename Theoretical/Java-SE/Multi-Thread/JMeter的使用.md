1. 首先官方下载apache-jmeter

2. 打开jmeter的bin目录中的jmeter.bat文件启动jmeter

3. Add -> Threads -> Thread Group 创建线程组

   ![image-20210803134200798](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-创建线程组.png)

4. 配置线程组参数，设置名称，设置线程数，设置线程投放总时间，设置循环次数

   ![image-20210803134318174](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-配置线程组.png)

5. 添加Http请求，并配置参数：如协议，请求方法，接口地址，ip地址，编码，参数等等

   ![image-20210803134919840](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-创建Http请求.png)

   ![image-20210803135002227](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-配置Http请求.png)

6. 创建总结报告元件

   ![image-20210803135045696](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-创建总结报告.png)

7. 若要模拟多用户请求，则创建CSV Data Set Config元件，并按照其规则进行参数文件配置

   ![image-20210803135121966](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-创建CSV数据配置元件.png)

   ![image-20210803135217365](../../../../picture/Theoretical/Java-SE/Multi-Thread/jmeter-多用户请求参数配置.png)

   此时还需要改进Http Request中的参数配置方式

   ![image-20210803135312417](C:/Users/Personal Computer/AppData/Roaming/Typora/typora-user-images/image-20210803135312417.png)