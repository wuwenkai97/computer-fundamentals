![image-20210627113222361](C:/Users/Personal Computer/AppData/Roaming/Typora/typora-user-images/image-20210627113222361.png)

* Collection和Map是所有集合框架的父接口
* Collection接口
  * 子接口Set、List和Queue
  * Set接口的实现类主要有HashSet、LinkedHashSet以及TreeSet
  * List接口的实现类主要有ArrayList、LinkedList、Stack以及Vector
  * Queue接口的实现类主要有Deque
* Map接口
  * 实现类主要有HashMap、TreeMap、ConcurrentHashMap以及Hashtable
* 抽象基类是以Abstract开头的Map、Collection为类名的类