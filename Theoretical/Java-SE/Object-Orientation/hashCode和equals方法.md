* hashCode()的作用是获取哈希码，也称散列码；它实际上是返回一个int整数。这个哈希码的作用是确定该对象在哈希表的索引位置。hashCode位于Object类中，这意味着，Java所有类均包含该方法

* hashCode()的意义：当你把对象加入HashSet时，HashSet会先计算对象的hashcode值来判断对象加入的位置，如果没有相符的hashCode，HashSet会判定对象没有重复出现；而一旦有相同的hashCode时，就采用equals()方法来比较两个对象是否真的相同，若相同，则对象插入失败，若不同，则会重新散列到其他位置，这样就大大减少了equals()方法的调用次数，提高了执行速度

* 如果没有重写hashCode()，则两个对象无论如何也不会相等，即使它们指向相同的数据（即两个equals相等的对象，它们的哈希值也必须相等）

* equals方法代码实现：首先比较对象的地址是否相同，然后比较对象所属类是否相同，最后再比较具体的属性，这样的代码更加健壮

```java
 public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }
        if (anObject instanceof String) {
            String anotherString = (String)anObject;
            int n = value.length;
            if (n == anotherString.value.length) {
                char v1[] = value;
                char v2[] = anotherString.value;
                int i = 0;
                while (n-- != 0) {
                    if (v1[i] != v2[i])
                        return false;
                    i++;
                }
                return true;
            }
        }
        return false;
    }
```

