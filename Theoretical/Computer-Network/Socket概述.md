Socket（[详解](https://www.jianshu.com/p/066d99da7cbd)）：**ip地址+协议+端口唯一标识一个进程**，Socket是对TCP/IP协议的抽象，是操作系统对外开发的接口

我们要理解网络中进程如何通信，得解决两个问题：

1. 我们要如何标识一台主机，即怎样确定我们将要通信的进程是在那一台主机上运行。
2. 我们要如何标识唯一进程，本地通过pid标识，网络中应该怎样标识？

解决办法：

1. TCP/IP协议族已经帮我们解决了这个问题，网络层的“ip地址”可以唯一标识网络中的主机
2. 传输层的“协议+端口”可以唯一标识主机中的应用程序（进程），因此，我们利用三元组（ip地址，协议，端口）就可以标识网络的进程了，网络中的进程通信就可以利用这个标志与其它进程进行交互

* Socket通信流程

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/Socket%E9%80%9A%E4%BF%A1%E6%B5%81%E7%A8%8B.png" alt="image-20210601235856641" style="zoom: 67%;" />

![image-20210708235427472](https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/TCP%E4%B8%89%E6%8F%A1%E5%9B%9B%E6%8C%A5%E7%9A%84%E6%B5%81%E7%A8%8B.png)

* TCP三次握手结合Socket：

  客户端调用 socket() 函数创建套接字后，因为没有建立连接，所以套接字处于CLOSED状态；服务器端调用 listen() 函数后，套接字进入LISTEN状态，开始监听客户端请求，这时客户端发起请求：

  1. 当客户端调用 connect() 函数后，TCP协议会组建一个数据包，并设置 SYN 标志位，表示该数据包是用来建立同步连接的。同时生成一个随机数字 1000，填充“序号（Seq）”字段，表示该数据包的序号。完成这些工作，开始向服务器端发送数据包，客户端就进入了SYN-SEND状态。

  2. 服务器端收到数据包，检测到已经设置了 SYN 标志位，就知道这是客户端发来的建立连接的“请求包”。服务器端也会组建一个数据包，并设置 SYN 和 ACK 标志位，SYN 表示该数据包用来建立连接，ACK 用来确认收到了刚才客户端发送的数据包

     > 服务器生成一个随机数 2000，填充“序号(Seq)”字段。2000 和客户端数据包没有关系。
     >  服务器将客户端数据包序号（1000）加1，得到1001，并用这个数字填充“确认号（Ack）”字段。
     >  服务器将数据包发出，进入SYN-RECV状态

  3. 客户端收到数据包，检测到已经设置了 SYN 和 ACK 标志位，就知道这是服务器发来的“确认包”。客户端会检测“确认号（Ack）”字段，看它的值是否为 1000+1，如果是就说明连接建立成功。接下来，客户端会继续组建数据包，并设置 ACK 标志位，表示客户端正确接收了服务器发来的“确认包”。同时，将刚才服务器发来的数据包序号（2000）加1，得到 2001，并用这个数字来填充“确认号（Ack）”字段。客户端将数据包发出，进入ESTABLISED状态，表示连接已经成功建立。
  
  4. 服务器端收到数据包，检测到已经设置了 ACK 标志位，就知道这是客户端发来的“确认包”。服务器会检测“确认号（Ack）”字段，看它的值是否为 2000+1，如果是就说明连接建立成功，服务器进入ESTABLISED状态。

  **至此，客户端和服务器都进入了ESTABLISED状态，连接建立成功，接下来就可以收发数据了。**

* TCP四次挥手结合Socket

  建立连接后，客户端和服务器都处于ESTABLISED状态。这时，客户端发起断开连接的请求：

  1. 客户端调用 close() 函数后，向服务器发送 FIN 数据包，进入FIN_WAIT_1状态。FIN 是 Finish 的缩写，表示完成任务需要断开连接。
  2. 服务器收到数据包后，检测到设置了 FIN 标志位，知道要断开连接，于是向客户端发送“确认包”，进入CLOSE_WAIT状态。
      注意：服务器收到请求后并不是立即断开连接，而是先向客户端发送“确认包”，告诉它我知道了，我需要准备一下才能断开连接。
  3. 客户端收到“确认包”后进入FIN_WAIT_2状态，等待服务器准备完毕后再次发送数据包。
  4. 等待片刻后，服务器准备完毕，可以断开连接，于是再主动向客户端发送 FIN 包，告诉它我准备好了，断开连接吧。然后进入LAST_ACK状态。
  5. 客户端收到服务器的 FIN 包后，再向服务器发送 ACK 包，告诉它你断开连接吧。然后进入TIME_WAIT状态。
  6. 服务器收到客户端的 ACK 包后，就断开连接，关闭套接字，进入CLOSED状态。

