ipv4地址概述

* IPv4地址就是给因特网（Internet）上的每一台主机的每一个接口分配一个在全世界范围内是唯一的32比特的标识符

* IPv4地址的编制方法经历了如下三个历史阶段

  * 分类编址，1984
  * 划分子网，1985
  * 无分类编址，1993

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E7%82%B9%E5%88%86%E5%8D%81%E8%BF%9B%E5%88%B6%E8%A1%A8%E7%A4%BA%E6%B3%95.png" alt="image-20210623220819188" style="zoom:50%;" />

* 分类编址的IPv4地址

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E5%88%86%E7%B1%BB%E7%BC%96%E5%8F%B7%E7%9A%84ipv4%E5%9C%B0%E5%9D%80.png" alt="image-20210623223738614" style="zoom:50%;" />

  * A类地址

    **本地环回测试：可以检测端口的状态是否正常，网络维护工作中常用于定位端口故障。**

    <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/A%E7%B1%BB%E5%9C%B0%E5%9D%80.png" alt="image-20210623224239169" style="zoom:50%;" />

  * B类地址

    <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/B%E7%B1%BB%E5%9C%B0%E5%9D%80.png" alt="image-20210623225553991" style="zoom:50%;" />

  * C类地址

    <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/C%E7%B1%BB%E5%9C%B0%E5%9D%80.png" alt="image-20210623225736086" style="zoom:50%;" />

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/C%E7%B1%BB%E5%9C%B0%E5%9D%80%E4%BE%8B%E9%A2%98.png" alt="image-20210623230335586" style="zoom:50%;" />

* 划分子网的IPv4地址

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E5%88%92%E5%88%86%E5%AD%90%E7%BD%91%E7%9A%84ip%E5%9C%B0%E5%9D%801.png" alt="image-20210623232446428" style="zoom:50%;" />

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E5%88%92%E5%88%86%E5%AD%90%E7%BD%91%E7%9A%84ip%E5%9C%B0%E5%9D%802.png" alt="image-20210623232547513" style="zoom:50%;" />

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E5%88%92%E5%88%86%E5%AD%90%E7%BD%91%E7%9A%84ip%E5%9C%B0%E5%9D%803.png" alt="image-20210623232807466" style="zoom:50%;" />

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E5%88%92%E5%88%86%E5%AD%90%E7%BD%91%E7%9A%84ip%E5%9C%B0%E5%9D%804.png" alt="image-20210623233418150" style="zoom:50%;" />

* 无分类编址的IPv4地址

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E6%97%A0%E5%88%86%E7%B1%BB%E7%BC%96%E5%9D%80%E7%9A%84ipv4%E5%9C%B0%E5%9D%80.png" alt="image-20210623233651149" style="zoom:50%;" />

  <img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/IP/%E6%97%A0%E5%88%86%E7%B1%BB%E7%BC%96%E5%9D%80%E7%9A%84ipv4%E5%9C%B0%E5%9D%80%E4%BE%8B%E9%A2%98.png" alt="image-20210623234021636" style="zoom:50%;" />

  

