# TCP三次握手

当应用程序希望通过TCP与另一个程序通信时它会发送一个通信请求，这个请求必须被送到一个确切的地址，在双方握手之后，TCP将在两个应用之间建立一个全双工的通信，全双工的通信（A可以给B发送信息，B也可以给A发送信息）会占用两个计算机之间的通信线路，直到它被一方或双方关闭为止

<img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/TCP-UDP/TCP%E4%B8%89%E6%AC%A1%E6%8C%A5%E6%89%8B%E7%A4%BA%E6%84%8F%E5%9B%BE.png" alt="image-20210531232234285" style="zoom:67%;" />

* 第一次握手

  一开始客户端和服务端都会处于CLOSED的状态，假设主动打开连接的是客户端，被动打开连接的是服务端，刚开始TCP服务进程先创建传输控制块TCB时刻准备接受其他客户进程发送过来的连接请求，此时服务端进入LISTEN状态，此时TCP客户端进程也是先创建一个传输控制块TCB，然后向服务器发出连接请求报文（包含同步标志SYN=1以及初始序号序列号x），此时客户进程进入SYN-SENT同步已发送状态，被发送出去的报文段被称为SYN报文段，它不能携带数据，但会消耗一个序号，以上就是第一次握手

* 第二次握手

  当服务器接收到请求报文，如果同意连接，则发出确认报文（包含SYN=1，ACK=1，出书画序列号seq=y以及ack=x+1），此时服务器进入SYN-RECEIVED即同步收到状态，这个报文无法携带数据，也会消耗掉一个序号

  **为何ack=x+1？**因为在之前第一次握手中的seq=x，作为回应，要回应和x相关的信息，由于上一个报文消耗掉了一个序号，因此ack=x+1

* 第三次握手

  当客户进程收到确认报文之后，还要向服务器给出一个确认报文（包含ACK=1，seq=x+1，ack=y+1原因同上），此时客户端进入ESTABLISHED状态，即已建立连接，TCP规定这个报文段可以携带数据，也可以不携带数据，这样就不会消耗掉序号，当服务器收到客户端的报文段后也会进入ESTABLISHED状态，此后双方开始通信

  **为何seq=x+1？**由于先前已经告知序号+1，因此回复一个seq=x+1到服务端

* 三次握手问题？

  * 为何需要三次握手才能建立连接

    1. 为了保证服务端能收接受到客户端的信息并能做出正确的应答而进行前两次(第一次和第二次)握手，为了保证客户端能够接收到服务端的信息并能做出正确的应答而进行后两次(第二次和第三次)握手。

    2. 为了初始化Sequence Number的初始值，通信的双方要通知对方自己的初始化的Sequence Number，即上图的x和y，这个序号要作为以后的数据通信的序号，以保证应用层接收到的数据不会因为网络传输问题而乱序，即TCP会用这个序号来拼接数据，因此，在服务器回发seq（第二次握手之后）还需要发送确认报文给服务器，告知服务器客户端已经收到初始化的seq。

  * 首次握手隐患——SYN超时问题：Server收到Client的SYN，回复SYN-ACK的时候未收到ACK确认（Client掉线），Server不断重试直至超时，Linux默认等待63秒（5次，1+2+4+8+16+32）才断开连接，但这样一来可能会有SYN Flood风险，恶意程序会给服务器发SYN报文，发完就下线，服务器需要等63秒才会断开连接，这样攻击者就会将服务器的SYN连接队列耗尽，让正常的连接请求无法处理

    针对SYN Flood的防护措施：SYN队列满后，通过tcp_syncookies参数回发SYN Cookie；若是正常连接则Client会回发SYN Cookie，直接建立连接，这样即使队列满了也可以建立连接

  * 建立连接后，Client出现故障怎么办

    保活机制：在保活时间内，连接若处于非活动状态，开启保活功能的一端向对方发送保活探测报文，如果未收到响应则继续发送，若尝试次数达到保活探测限制数而仍未收到响应则中断连接

## TCP的四次挥手

* 第一次挥手

  初始时客户端和服务端都处于ESTABLISHED状态，假设客户端主动关闭，服务端被动关闭，客户端进程发出连接释放报文，并且停止发送数据（报文段包含FIN=1以及seq=u，u为前面ESTABLISHED状态下数据最后一次发送时已经传送过来的数据的最后一个字节的序号+1），此时客户端进入FIN-WAIT-1状态，TCP规定即使FIN报文段不携带数据也要消耗一个序号

* 第二次挥手

  服务器收到连接释放报文，也要发出确认报文（包含ACK=1，seq=v，ack=u+1），此时，服务器进入CLOSE-WAIT状态，TCP通知服务器应用进程要关闭连接，这时会处于半关闭状态，服务器还有数据要传送，但客户端已经没有数据传输了，因此还需要持续整个CLOSE-WAIT状态所持续的时间，当客户端收到服务器的确认报文后，客户端就进入FIN-WAIT-2状态，等待服务器发送释放连接报文，这段时间还会接收服务器发的最后一段数据

* 第三次挥手

  服务端数据发送完毕后就会向客户端发送连接释放报文（包含FIN=1，ACK=1，seq=w，w表示在版关闭状态下服务器发送的最后一段数据的最后一个字节的序号+1，ack=u+1），此时服务器进入LAST-ACK这样一个状态，等待客户端的最终确认

* 第四次挥手

  客户端发出确认收到的报文（报文段包含ACK=1，seq=u+1.ack=w+1），服务收到客户端的确认报文后进入CLOSED状态，客户端此时进入TIME-WAIT状态，必须等待2MSL（最长报文段寿命的2倍）的时间才会进入CLOSED状态，完成四次挥手

<img src="https://gitee.com/wuwenkai97/picture/raw/master/Theoretical/Computer-Network/TCP-UDP/TCP%E5%9B%9B%E6%AC%A1%E6%8F%A1%E6%89%8B%E7%A4%BA%E6%84%8F%E5%9B%BE.png" alt="image-20210601111004673" style="zoom:80%;" />



* 四次挥手的问题

  * 为什么会有TIME_WAIT状态

    1. TIME_WAIT状态保证有足够的时间让对方收到ACK包，如果被动端没有收到ACK报文，就会重发FIN包，一来一去正好2MSL
    2. 有足够的时间避免新旧连接混淆，有些路由器会缓存IP数据包，如果连接被重用了，那么延迟收到的包可能会跟新连接混在一起

  * 为什么需要四次挥手终端连接

    因为全双工通信，发送方和接收方都需要FIN报文和ACK报文，因此发送方和接收方各两次挥手即可，所以看上去就是所谓的四次挥手

  * 服务器出现大量CLOSE_WAIT状态的原因

    现象：客户端一直在请求，但是返回给客户端的信息是异常的，服务端没有收到请求

    对方关闭socket连接，我方忙于读和写，没有及时关闭连接

    1. 检查代码，特别是释放资源的代码
    2. 检查配置，特别是处理请求的线程配置不合理

