# SQL注入的风险

刚刚讲过当我们访问动态网页时, Web 服务器会向数据访问层发起 Sql 查询请求，如果权限验证通过就会执行 Sql 语句。
 这种网站内部直接发送的Sql请求一般不会有危险，但实际情况是很多时候需要**结合**用户的输入数据动态构造 Sql 语句，如果用户输入的数据被构造成恶意 Sql 代码，Web 应用又未对动态构造的 Sql 语句使用的参数进行审查，则会带来意想不到的危险。

Sql 注入带来的威胁主要有如下几点

- 猜解后台数据库，这是利用最多的方式，盗取网站的敏感信息。
- 绕过认证，列如绕过验证登录网站后台。
- 注入可以借助数据库的存储过程进行提权等操作

# SQL注入案例

## 案例一：猜测数据库

接下来我们通过一个实例，让你更加清楚的理解 **Sql 注入猜解数据库**是如何发生的。
使用DVWA渗透测试平台，作为攻击测试的目标：

![img](https:////upload-images.jianshu.io/upload_images/6230889-bec6fd7c9345279a.png?imageMogr2/auto-orient/strip|imageView2/2/w/895/format/webp)

先输入 1 ，查看回显 (URL中ID=1，说明php页面通过get方法传递参数)：

![img](https:////upload-images.jianshu.io/upload_images/6230889-b353561b5a5a9f5c.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

那实际上后台执行了什么样的Sql语句呢？点击 `view source`查看源代码 ，其中的SQL查询代码为：

![img](https:////upload-images.jianshu.io/upload_images/6230889-b644383d5869374c.png?imageMogr2/auto-orient/strip|imageView2/2/w/816/format/webp)

可以看到，实际执行的Sql语句是：

```mysql
SELECT first_name, last_name FROM users WHERE user_id = '1';
```

我们是通过控制参数Id的值来返回我们需要的信息。
 如果我们不按常理出牌，比如在输入框中输入 `1' order by 1#`
 实际执行的Sql语句就会变成:

```mysql
SELECT first_name, last_name FROM users WHERE user_id = '1' order by 1#`; # (按照Mysql语法，#后面会被注释掉，使用这种方法屏蔽掉后面的单引号，避免语法错误)
```

这条语句的意思是查询users表中user_id为1的数据并按第一字段排行。

输入 `1' order by 1#`和 `1' order by 2#`时都返回正常：

![img](https:////upload-images.jianshu.io/upload_images/6230889-a34bf774595fbb48.png?imageMogr2/auto-orient/strip|imageView2/2/w/659/format/webp)

![img](https:////upload-images.jianshu.io/upload_images/6230889-a7c16d9dbf698edb.png?imageMogr2/auto-orient/strip|imageView2/2/w/662/format/webp)

当输入 `1' order by 3#`时，返回错误：

![img](https:////upload-images.jianshu.io/upload_images/6230889-256a7d9b495a0587.png?imageMogr2/auto-orient/strip|imageView2/2/w/669/format/webp)

**由此可知，users表中只有两个字段，数据为两列。**

## 案例二：绕过验证

接下来我们再试试另一个利用 **Sql 漏洞绕过登录验证**的实例。
 使用事先编写好的页面，这是一个普通的登录页面，只要输入正确的用户名和密码就能登录成功。

![img](https:////upload-images.jianshu.io/upload_images/6230889-f28e32057ec5a979.png?imageMogr2/auto-orient/strip|imageView2/2/w/501/format/webp)

我们先尝试随意输入用户名 123 和密码 123 登录：

![img](https:////upload-images.jianshu.io/upload_images/6230889-8b4414ab9fe44cc2.png?imageMogr2/auto-orient/strip|imageView2/2/w/503/format/webp)

从错误页面中我们无法获取到任何信息。
看看后台代码如何做验证的：

![img](https:////upload-images.jianshu.io/upload_images/6230889-156009a2bc7e79d1.png?imageMogr2/auto-orient/strip|imageView2/2/w/670/format/webp)

实际执行的操作时：

```mysql
select * from users where username='123' and password='123'
```

当查询到数据表中存在同时满足 username 和 password 字段时，会返回登录成功。
 按照第一个实例的思路，我们尝试在用户名中输入 `123' or 1=1 #`, 密码同样输入 `123' or 1=1 #` ：

![img](https:////upload-images.jianshu.io/upload_images/6230889-f9bfece283965162.png?imageMogr2/auto-orient/strip|imageView2/2/w/503/format/webp)

![img](https:////upload-images.jianshu.io/upload_images/6230889-c1be76d1c05814fd.png?imageMogr2/auto-orient/strip|imageView2/2/w/501/format/webp)

为什么能够成功登陆呢？因为实际执行的语句是：

```mysql
select * from users where username='123' or 1=1 #' and password='123' or 1=1 #'
```

按照 Mysql 语法，# 后面的内容会被忽略，所以以上语句等同于（实际上密码框里不输入任何东西也一样）：

```mysql
select * from users where username='123' or 1=1 
```

由于判断语句 or 1=1 恒成立，所以结果当然返回真，成功登录。