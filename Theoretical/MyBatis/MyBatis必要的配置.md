# MyBatis必要配置

`mybatis.type-aliase-package`: 数据表的JavaBean模型所在的包

`mybatis.configuration.map-underscore-to-camel-case`: 将带有下划线的表字段映射为驼峰格式的实体类属性

`mybatis.configuration.default-fetch-size`: 默认连接数量

`mybatis.configuration.default-statement-timeout`: 超时时间

`mybatis.mapperLocations`: mapper.xml文件所在位置

# 数据库连接池必要配置

`spring.datasource.driver-class-name`: 驱动名称

`spring.datasource.password`: 数据库密码

`spring.datasource.username`: 数据库用户名

`spring.datasource.url`: 数据库url地址

`spring.datasource.type`: 数据源类型

