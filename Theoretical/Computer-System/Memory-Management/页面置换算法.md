* 最佳置换算法

  每次选择淘汰的页面将是以后用不使用，或者在最长时间内不再被访问的页面，这样可以保证最低的缺页率

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/最佳置换算法.png" alt="image-20210620114051775" style="zoom:50%;" />

* 先进先出置换算法

  每次淘汰的页面是最早进入内存的页面

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/先进先出算法.png" alt="image-20210620114158169" style="zoom:50%;" />

* LRU

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/LRU算法.png" alt="image-20210620114249559" style="zoom:50%;" />

* 时钟置换算法（CLOCK）

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/时钟置换算法.png" alt="image-20210620114432969" style="zoom:50%;" />

  <img src="C:\Users\Personal Computer\AppData\Roaming\Typora\typora-user-images\image-20210620114615891.png" alt="image-20210620114615891" style="zoom:50%;" />

