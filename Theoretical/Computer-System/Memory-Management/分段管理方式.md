# 分段管理方式

* 分段

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/分段管理说明.png" alt="image-20210619231305014" style="zoom:50%;" />

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/分段地址的逻辑系统结构.png" alt="image-20210619231808398" style="zoom:50%;" />

* 段表

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/段表.png" alt="image-20210619232010150" style="zoom:50%;" />

* 地址变换

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/分段地址变换流程.png" alt="image-20210619232414761" style="zoom:50%;" />

* 分段和分页的对比

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/分段和分页的区别.png" alt="image-20210619232732518" style="zoom:50%;" />

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/信息共享和保护案例1.png" alt="image-20210619233048724" style="zoom:50%;" />

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/信息共享和保护案例2.png" alt="image-20210619233301083" style="zoom:50%;" />

  <img src="../../../../picture/Theoretical/Computer-System/Memory-Management/访问一个逻辑地址要几次访存.png" alt="image-20210619233347583" style="zoom:50%;" />

# 段页式管理方式

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/段页式逻辑地址结构.png" alt="image-20210620103250726" style="zoom:50%;" />

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/段页式访问流程.png" alt="image-20210620103939524" style="zoom:50%;" />