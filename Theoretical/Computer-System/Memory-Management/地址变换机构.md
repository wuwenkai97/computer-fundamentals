### 基本地址变换机构

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/基本地址变换机构介绍.png" alt="image-20210618225343842" style="zoom:50%;" />

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/地址变换流程.png" alt="image-20210618225632640" style="zoom:50%;" />

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/地址变换流程说明.png" alt="image-20210618225923864" style="zoom:50%;" />

### 具有快表的地址变换机构

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/局部性原理.png" alt="image-20210618230658434" style="zoom:50%;" />

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/快表的存储流程.png" alt="image-20210618231051602" style="zoom:50%;" />

​								

<img src="../../../../picture/Theoretical/Computer-System/Memory-Management/快表的存储流程说明.png" alt="image-20210618231334001" style="zoom:50%;" />

