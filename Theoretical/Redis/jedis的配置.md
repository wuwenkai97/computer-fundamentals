# redis配置

```yaml
redis.host=192.168.109.130 # ip地址
redis.port=6379 # 端口号
redis.timeout=10 # 连接过期时间，在这段时间内没有交互则断开连接
redis.password=123456 # 密码
redis.poolMaxTotal=1000 # 最大线程数
redis.poolMaxIdle=500 # 空闲的最大线程数
```

# jedis配置

```java
@Autowired
RedisConfig redisConfig;

@Bean
public JedisPool JedisPoolFactory() {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxIdle(redisConfig.getPoolMaxIdle());
    poolConfig.setMaxTotal(redisConfig.getPoolMaxTotal());
    poolConfig.setMaxWaitMillis(redisConfig.getPoolMaxWait() * 1000);
    return new JedisPool
            (
                    poolConfig,
                    redisConfig.getHost(),
                    redisConfig.getPort(),
                    redisConfig.getTimeout() * 1000,
                    redisConfig.getPassword(),
                    0
            );
}
```