# 数据类型

![image-20210704234308754](../../../picture/Theoretical/Redis/redis数据类型.png)

* string

  **string** 是 redis 最基本的类型，你可以理解成与 Memcached 一模一样的类型，一个 key 对应一个 value。value其实不仅是String，也可以是数字。string 类型是二进制安全的。意思是 redis 的 string 可以包含任何数据。比如jpg图片或者序列化的对象。string 类型是 Redis 最基本的数据类型，string 类型的值最大能存储 512MB。

  常用命令：get、set、incr、decr、mget等。

  应用场景：String是最常用的一种数据类型，普通的key/ value 存储都可以归为此类，即可以完全实现目前 Memcached 的功能，并且效率更高。还可以享受Redis的定时持久化，操作日志及 Replication等功能。除了提供与 Memcached 一样的get、set、incr、decr 等操作外，Redis还提供了下面一些操作： 

  * 获取字符串长度
  * 往字符串append内容
  * 设置和获取字符串的某一段内容
  * 设置及获取字符串的某一位（bit）
  * 批量设置一系列字符串的内容
  
  使用场景：常规key-value缓存应用。常规计数: 微博数, 粉丝数。
  
  ```shell
  redis 127.0.0.1:6379> SET name "runoob"
  "OK"
  redis 127.0.0.1:6379> GET name
  "runoob"
  ```
  
* hash 

  **Redis hash** 是一个键值(key => value)对集合。Redis hash 是一个 string 类型的 field 和 value 的映射表，hash 特别适合用于存储对象。

  常用命令：hget,hset,hgetall 等。

  应用场景：我们简单举个实例来描述下Hash的应用场景，比如我们要存储一个用户信息对象数据，包含以下信息：

  ​		用户ID为查找的key，存储的value用户对象包含姓名，年龄，生日等信息，如果用普通的key/value结构来存储，主要有以下2种存储方式：

  ![img](https://images2018.cnblogs.com/blog/1368782/201808/1368782-20180821202451984-479691318.png)

  　　第一种方式将用户ID作为查找key,把其他信息封装成一个对象以序列化的方式存储，这种方式的缺点是，增加了序列化/反序列化的开销，并且在需要修改其中一项信息时，需要把整个对象取回，并且修改操作需要对并发进行保护，引入CAS等复杂问题。

  ![img](https://images2018.cnblogs.com/blog/1368782/201808/1368782-20180821202546802-636845502.png)

  　　第二种方法是这个用户信息对象有多少成员就存成多少个key-value对儿，用用户ID+对应属性的名称作为唯一标识来取得对应属性的值，虽然省去了序列化开销和并发问题，但是用户ID为重复存储，如果存在大量这样的数据，内存浪费还是非常可观的。

  　　那么Redis提供的Hash很好的解决了这个问题，Redis的Hash实际是内部存储的Value为一个HashMap，并提供了直接存取这个Map成员的接口，如下图：

  ![img](https://images2018.cnblogs.com/blog/1368782/201808/1368782-20180821202632663-939669694.png)

  　　也就是说，Key仍然是用户ID, value是一个Map，这个Map的key是成员的属性名，value是属性值，这样对数据的修改和存取都可以直接通过其内部Map的Key(Redis里称内部Map的key为field), 也就是通过 key(用户ID) + field(属性标签) 就可以操作对应属性数据了，既不需要重复存储数据，也不会带来序列化和并发修改控制的问题，很好的解决了问题。

  　　这里同时需要注意，Redis提供了接口(hgetall)可以直接取到全部的属性数据，但是如果内部Map的成员很多，那么涉及到遍历整个内部Map的操作，由于Redis单线程模型的缘故，这个遍历操作可能会比较耗时，而另其它客户端的请求完全不响应，这点需要格外注意。

  　　使用场景：存储部分变更数据，如用户信息等。

  ```shell
  127.0.0.1:6379>  HMSET runoobkey name "redis tutorial" description "redis basic commands for caching" likes 20 visitors 23000
  OK
  127.0.0.1:6379>  HGETALL runoobkey
  1) "name"
  2) "redis tutorial"
  3) "description"
  4) "redis basic commands for caching"
  5) "likes"
  6) "20"
  7) "visitors"
  8) "23000"
  ```

* list

  **Redis list** 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）。

  常用命令：lpush（添加左边元素）,rpush,lpop（移除左边第一个元素）,rpop,lrange（获取列表片段，LRANGE key start stop）等。

  应用场景：Redis list的应用场景非常多，也是Redis最重要的数据结构之一，比如twitter的关注列表，粉丝列表等都可以用Redis的list结构来实现。

  List 就是链表，相信略有数据结构知识的人都应该能理解其结构。使用List结构，我们可以轻松地实现最新消息排行等功能。List的另一个应用就是消息队列，
  可以利用List的PUSH操作，将任务存在List中，然后工作线程再用POP操作将任务取出进行执行。Redis还提供了操作List中某一段的api，你可以直接查询，删除List中某一段的元素。

  实现方式：Redis list的实现为一个双向链表，即可以支持反向查找和遍历，更方便操作，不过带来了部分额外的内存开销，Redis内部的很多实现，包括发送缓冲队列等也都是用的这个数据结构。

  Redis的list是每个子元素都是String类型的双向链表，可以通过push和pop操作从列表的头部或者尾部添加或者删除元素，这样List即可以作为栈，也可以作为队列。 获取越接近两端的元素速度越快，但通过索引访问时会比较慢。

  使用场景：

  消息队列系统：使用list可以构建队列系统，使用sorted set甚至可以构建有优先级的队列系统。比如：将Redis用作日志收集器，实际上还是一个队列，多个端点将日志信息写入Redis，然后一个worker统一将所有日志写到磁盘。

  ```shell
  redis 127.0.0.1:6379> lpush runoob redis
  (integer) 1
  redis 127.0.0.1:6379> lpush runoob mongodb
  (integer) 2
  redis 127.0.0.1:6379> lpush runoob rabitmq
  (integer) 3
  redis 127.0.0.1:6379> lrange runoob 0 10
  1) "rabitmq"
  2) "mongodb"
  3) "redis"
  redis 127.0.0.1:6379>
  ```

* set

  **Redis set**是string类型的无序集合。集合是通过hashtable实现的，概念和数学中的集合基本类似，可以交集，并集，差集等等，set中的元素是没有顺序的。所以添加，删除，查找的复杂度都是O(1)。

  *sadd 命令：*添加一个 string 元素到 key 对应的 set 集合中，成功返回1，如果元素已经在集合中返回 0，如果 key 对应的 set 不存在则返回错误。

  常用命令：sadd,spop,smembers,sunion 等。

  应用场景：Redis set对外提供的功能与list类似是一个列表的功能，特殊之处在于set是可以自动排重的，当你需要存储一个列表数据，又不希望出现重复数据时，set是一个很好的选择，并且set提供了判断某个成员是否在一个set集合内的重要接口，这个也是list所不能提供的。

  Set 就是一个集合，集合的概念就是一堆不重复值的组合。利用Redis提供的Set数据结构，可以存储一些集合性的数据。

  案例：在微博中，可以将一个用户所有的关注人存在一个集合中，将其所有粉丝存在一个集合。Redis还为集合提供了求交集、并集、差集等操作，可以非常方便的实现如共同关注、共同喜好、二度好友等功能，对上面的所有集合操作，你还可以使用不同的命令选择将结果返回给客户端还是存集到一个新的集合中。

  实现方式： set 的内部实现是一个 value永远为null的HashMap，实际就是通过计算hash的方式来快速排重的，这也是set能提供判断一个成员是否在集合内的原因。

  ```shell
  redis 127.0.0.1:6379> sadd runoob redis
  (integer) 1
  redis 127.0.0.1:6379> sadd runoob mongodb
  (integer) 1
  redis 127.0.0.1:6379> sadd runoob rabitmq
  (integer) 1
  redis 127.0.0.1:6379> sadd runoob rabitmq
  (integer) 0
  redis 127.0.0.1:6379> smembers runoob
  1) "redis"
  2) "rabitmq"
  3) "mongodb"
  ```

* zset

  **redis zset** 和 set 一样也是string类型元素的集合,且不允许重复的成员。

  zadd 命令：添加元素到集合，元素在集合中存在则更新对应score。

  常用命令：zadd,zrange,zrem,zcard等

  使用场景：Redis sorted set的使用场景与set类似，区别是set不是自动有序的，而sorted set可以通过用户额外提供一个优先级(score)的参数来为成员排序，并且是插入有序的，即自动排序。当你需要一个有序的并且不重复的集合列表，那么可以选择sorted set数据结构，比如twitter 的public timeline可以以发表时间作为score来存储，这样获取时就是自动按时间排好序的。和Set相比，**Sorted Set关联了一个double类型权重参数score**，使得集合中的元素能够按score进行有序排列，redis正是通过分数来为集合中的成员进行从小到大的排序。zset的成员是唯一的,但分数(score)却可以重复。比如一个存储全班同学成绩的Sorted Set，其集合value可以是同学的学号，而score就可以是其考试得分，这样在数据插入集合的时候，就已经进行了天然的排序。另外还可以用Sorted Set来做带权重的队列，比如普通消息的score为1，重要消息的score为2，然后工作线程可以选择按score的倒序来获取工作任务。让重要的任务优先执行。

  实现方式：Redis sorted set的内部使用HashMap和跳跃表(SkipList)来保证数据的存储和有序，HashMap里放的是成员到score的映射，而跳跃表里存放的是所有的成员，排序依据是HashMap里存的score,使用跳跃表的结构可以获得比较高的查找效率，并且在实现上比较简单。

  ```shell
  redis 127.0.0.1:6379> zadd runoob 0 redis
  (integer) 1
  redis 127.0.0.1:6379> zadd runoob 0 mongodb
  (integer) 1
  redis 127.0.0.1:6379> zadd runoob 0 rabitmq
  (integer) 1
  redis 127.0.0.1:6379> zadd runoob 0 rabitmq
  (integer) 0
  redis 127.0.0.1:6379> > ZRANGEBYSCORE runoob 0 1000
  1) "mongodb"
  2) "rabitmq"
  3) "redis"
  ```

# 跳表底层逻辑

下图是一个简单的**有序单链表**，单链表的特性就是每个元素存放下一个元素的引用。即：通过第一个元素可以找到第二个元素，通过第二个元素可以找到第三个元素，依次类推，直到找到最后一个元素。

![img](https:////upload-images.jianshu.io/upload_images/19063731-70b00aafa9f5b793.jpeg?imageMogr2/auto-orient/strip|imageView2/2/w/1142/format/webp)

​                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     现在我们有个场景，想快速找到上图链表中的 10 这个元素，只能从头开始遍历链表，直到找到我们需要找的元素。查找路径：1、3、4、5、7、8、9、10。这样的查找效率很低，平均时间复杂度很高O(n)。那有没有办法提高链表的查找速度呢？如下图所示，我们从链表中每两个元素抽出来，加一级索引，一级索引指向了原始链表，即：通过一级索引 7 的down指针可以找到原始链表的 7 。那现在怎么查找 10 这个元素呢？

![img](https:////upload-images.jianshu.io/upload_images/19063731-4f4535e6d0959c32.jpeg?imageMogr2/auto-orient/strip|imageView2/2/w/1142/format/webp)

先在索引 1、4、7、9，遍历到一级索引的 9 时，发现 9 的后继节点是 13，比 10 大，于是不往后找了，而是通过 9 找到原始链表的 9，然后再往后遍历找到了我们要找的 10，遍历结束。有没有发现，加了一级索引后，查找路径：1、4、7、9、10，查找节点需要遍历的元素相对少了，我们不需要对 10 之前的所有数据都遍历，查找的效率提升了。

那如果加二级索引呢？如下图所示，查找路径：1、7、9、10。是不是找 10 的效率更高了？这就是跳表的思想，用“空间换时间”，通过给链表建立索引，提高了查找的效率。

![img](https:////upload-images.jianshu.io/upload_images/19063731-3852cc36af701f46.jpeg?imageMogr2/auto-orient/strip|imageView2/2/w/1142/format/webp)

可能同学们会想，从上面案例来看，提升的效率并不明显，本来需要遍历8个元素，优化了半天，还需要遍历 4 个元素，其实是因为我们的数据量太少了，当数据量足够大时，效率提升会很大。如下图所示，假如有序单链表现在有1万个元素，分别是 0~9999。现在我们建了很多级索引，最高级的索引，就两个元素 0、5000，次高级索引四个元素 0、2500、5000、7500，依次类推，当我们查找 7890 这个元素时，查找路径为 0、5000、7500 ... 7890，通过最高级索引直接跳过了5000个元素，次高层索引直接跳过了2500个元素，**从而使得链表能够实现二分查找**。由此可以看出，当元素数量较多时，索引提高的效率比较大，近似于二分查找。

![img](https:////upload-images.jianshu.io/upload_images/19063731-d7bc5026051ea412.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)