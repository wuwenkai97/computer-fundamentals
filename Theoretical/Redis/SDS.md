# SDS结构的定义

```c
  struct sdshdr{
    // 数组中已保存的字符长度
    int len;
    // 数组中还未使用的长度
    int free;
    // 字切数组，用于保存字符串
    char buf[];
  }
```

 在buf数组中，最后一位永远都是`/0 `,这一位也不会记入数组的长度中，对我们使用者来说，可以完全当它不存在，但是这个`/n` 存在的好处却是非常大的，因为C语言的字符串本来未尾就自带了一个 `/0` ，Redis 的SDS 后面也加一个数组 ，可以使Redis 完全可以重用C语言字符串的API。

# SDS和C语言原生字符串的比较

1. 获取字符串长度

   原生字符串获取长度的时间复杂度为 O(n) , 因为它没有记录自身长度的字段，所以如果想要获取字符串的长度，就必须要遍历整个字符串，直到遇到 `/0` 为止，才会返回，而SDS的 len 属性中本来就维护了一个自身长度的属性，所以获取 SDS 长度的时间复杂度为 o(1)。

   并且 SDS 的统计字符串的长度工作是在执行APi的时候 ，自动执行的，并且不需要我们手动设置，可以方便我们的使用，所以这保证了获取字符串长度的工作不会使Redis 陷入性能瓶颈，就算一直对一个字符串一直使用 STRLEN 命令，也不会造成任何影响 ，因为Redis 获取字符串长度的命令，时间复杂度仅为 O(1)。

2. 保证缓存区不会溢出

   使用C语言原生字符串的害处就是，如果修改一个字符串的时候，忘记给他重新分配空间，可能导致缓冲区益出，并且会覆盖别的数据。

   在使用SDS的时候 ，则不会出现这个情况，因为每一次修改SDS字符串的时候 ，都会先检查free属性，查看空间是否足够。如果不够的话，则会先对SDS进行扩容，然后才是对字符串进行修改。

    如果是在别的应用中，如果每一次修改字符串时去扩容或者缩容，是完全可以理解的，但是Redis是一个数据库，无法忍受这样性能浪费，所以每一次扩容的时候，都会为下一次修改预留空间，分配的规则如下：	

   > 如果SDS 的长度小于 1M，那么每一次增加的空间长度为此时len的长度，增加后 free 和 len 的长度应该是一样的。此时的长度计	算为: free + len + 1如果SDS 的长度大于 1M ，那么每一次增加 1M 的空闲空间，此时SDS的长度为：len + 1M + 1

   因为通过空间预分配，所以Redis 可以减少连续执行字符串增长操作所需要的内存重新分配次数，并且这些分配的空间，并不会因为字符串的缩短而被回收，而是会留在SDS中，以防止未来会有这一方面的需要。并且我们也不需要担心这些空间会对内存造成什么影响，SDS 的APi会在真正有用的时候 ，对这些空间进行释放。

# 二进制安全

我们在使用Redis 的时候 ，可以直接保存图片，视频，音乐。那是因为Redis存在buf里面的数据是二进制的，并不是简单的字符串，也不会对二进制数据进行处理，过滤等操作，这也保证了，我们存入数据的时候是什么样，读取数据的时候就是什么样。