package com.computer.util;

public class Constant {

    // 数字常量
    public static final int DEFAULT_LINKED_LIST_LENGTH = 10;

    public static final int LINKED_LIST_MIN_LENGTH = 0;

    public static final int SEED = 1234;

    public static final int TREE_VALUE_RANGE = 50;

    public static final int DEFAULT_TREE_DEPTH = 5;

    public static final int INITIAL_TREE_DEPTH = 0;

    public static final int DEFAULT_MULTI_TREE_CHILDREN_COUNT = 4;

    public static final int DEFAULT_TRIE_TREE_NODE_SIZE = 128;

    public static final int ARRAY_LENGTH_RANGE = 50;

    public static final int DEFAULT_ARRAY_LENGTH = 12;

    public static final int DEFAULT_UNION_FIND_SET_LENGTH = 26;

    public static final int GOODS_COUNT = 8;

    // 字符串常量
    public static final String WHITE = "WHITE";

    public static final String BLACK = "BLACK";

    public static final String LINKED_LIST_LENGTH_NOT_MEET_THE_SPECIFICATION = "链表长度不符合规范！";

    public static final String LINKED_LIST_LENGTH_ZERO = "链表长度为0！";

    public static final String LINKED_LIST_INDEX_OUT_OF_RANGE = "下标越界，无法操作！";

    public static final String SINGLE_LINKED_LIST_SPLIT_SIGNAL = "->";

    public static final String DOUBLE_LINKED_LIST_SPLIT_SIGNAL = "<=>";

    public static final String STRING_IS_NOT_EXIST = "字符串不存在！";

    public static final String STRING_LENGTH_ZERO = "字符串长度为0！";

    public static final String STRING_IS_ALREADY_EXIST = "字符串已存在！";

    public static final String HEAP_IS_NULL = "堆为空！";

    public static final String HEAP_IS_NULL_AND_LENGTH_ZERO = "堆为空或者长度为零，无法进行下沉操作！";

    public static final String PAGE_IS_NOT_EXIST = "页面不存在！";

    public static final String HEAD_SENTINEL = "头部哨兵";

    public static final String TAIL_SENTINEL = "尾部哨兵";

    public static final String MAIN_STRING_IS_NULL = "主串为空，无法匹配！";

    public static final String PATTERN_IS_NULL = "模式串为空，无法匹配！";

    public static final String MAIN_STRING_LESS_THAN_PATTERN_IN_LENGTH = "模式串长度大于主串，无法匹配！";

    public static final String STORAGE_STACK_IS_NULL = "存储栈为空！";

    public static final String CACHE_CAPACITY_ZERO = "缓存容量为0！";

    public static final String PARAMETER_IS_NULL = "参数为空！";

    // 数组常量
    public static final String[] DEFAULT_STRING_DICT = new String[] {"I", "love", "my", "wife", "and", "you"};

    public static final String[] UNION_FIND_SET_TEST = new String[] {"c==c","b==d","x!=z"};

    public static final int[] DEFAULT_THREE_SUM_TEST = new int[] {-1, 0, 1, 2, -1, -4};

}