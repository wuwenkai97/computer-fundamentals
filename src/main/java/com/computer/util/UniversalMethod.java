package com.computer.util;

import com.computer.fundamentals.datastructure.*;
import com.computer.fundamentals.datastructure.node.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class UniversalMethod {

    /**
     * 创建单向链表
     */
    public static SingleLinkedList createSingleLinkedList(int length) {
        if (length < Constant.LINKED_LIST_MIN_LENGTH) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_NOT_MEET_THE_SPECIFICATION);
        }
        SingleLinkedNode head = new SingleLinkedNode(Integer.MAX_VALUE);
        SingleLinkedList singleLinkedList = new SingleLinkedList(head);

        for (int i = 0;i < length;i++) {
            singleLinkedList.addFirst(i);
        }
        return singleLinkedList;
    }

    /**
     * 创建单向循环链表
     */
    public static SingleCircularLinkedList createSingleCircularLinkedList(int length) {
        if (length < Constant.LINKED_LIST_MIN_LENGTH) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_NOT_MEET_THE_SPECIFICATION);
        }

        // 设置头部哨兵
        SingleCircularLinkedNode head = new SingleCircularLinkedNode(Integer.MAX_VALUE);

        SingleCircularLinkedList singleCircularLinkedList = new SingleCircularLinkedList(head, null);

        for (int i = 0;i < length;i++) {
            singleCircularLinkedList.addFirst(i);
        }

        return singleCircularLinkedList;
    }

    /**
     * 打印单向链表
     */
    public static void printSingleLinkedList(SingleLinkedNode singleLinkedNode) {
        SingleLinkedNode cur = singleLinkedNode;
        while (cur != null) {
            if (cur.next != null) {
                System.out.print(cur.val + Constant.SINGLE_LINKED_LIST_SPLIT_SIGNAL);
            }
            else {
                System.out.print(cur.val);
            }
            cur = cur.next;
        }
    }

    /**
     * 创建双向链表
     */
    public static DoubleLinkedList createDoubleLinkedList(int length) {
        if (length < Constant.LINKED_LIST_MIN_LENGTH) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_NOT_MEET_THE_SPECIFICATION);
        }

        // 建立哨兵
        DoubleLinkedNode head = new DoubleLinkedNode(Integer.MAX_VALUE);
        DoubleLinkedNode tail = new DoubleLinkedNode(Integer.MAX_VALUE);

        DoubleLinkedList doubleLinkedList = new DoubleLinkedList(head, tail);

        for (int i = 0;i < length;i++) {
            doubleLinkedList.addFirst(i);
        }

        return doubleLinkedList;
    }

    /**
     * 创建双向循环链表
     */
    public static DoubleCircularLinkedList createDoubleCircularLinkedList(int length) {
        if (length < Constant.LINKED_LIST_MIN_LENGTH) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_NOT_MEET_THE_SPECIFICATION);
        }

        // 建立哨兵
        DoubleCircularLinkedNode head = new DoubleCircularLinkedNode(Integer.MAX_VALUE);
        DoubleCircularLinkedNode tail = new DoubleCircularLinkedNode(Integer.MAX_VALUE);

        DoubleCircularLinkedList doubleCircularLinkedList = new DoubleCircularLinkedList(head, tail);

        for (int i = 0;i < length;i++) {
            doubleCircularLinkedList.addFirst(i);
        }
        return doubleCircularLinkedList;
    }

    public static Random randomFactory = new Random(Constant.SEED);

    /**
     * 创建二叉树
     */
    public static BinaryTree createBinaryTree() {
        BinaryTreeNode root = new BinaryTreeNode(randomFactory.nextInt(Constant.TREE_VALUE_RANGE));
        int depth = Constant.INITIAL_TREE_DEPTH;
        Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (depth < Constant.DEFAULT_TREE_DEPTH) {
            int n = queue.size();
            for (int i = 0;i < n;i++) {
                BinaryTreeNode node = queue.poll();
                assert node != null;
                node.left = new BinaryTreeNode(randomFactory.nextInt(Constant.TREE_VALUE_RANGE));
                node.right = new BinaryTreeNode(randomFactory.nextInt(Constant.TREE_VALUE_RANGE));
                queue.offer(node.left);
                queue.offer(node.right);
            }
            depth++;
        }

        return new BinaryTree(root);
    }

    /**
     * 创建多叉树
     */
    public static MultiTree createMultiTree() {
        MultiTreeNode root = new MultiTreeNode(randomFactory.nextInt(Constant.TREE_VALUE_RANGE));

        Queue<MultiTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int depth = Constant.INITIAL_TREE_DEPTH;
        while (depth < Constant.DEFAULT_TREE_DEPTH) {
            int n = queue.size();
            for (int i = 0;i < n;i++) {
                MultiTreeNode node = queue.poll();
                assert node != null;
                List<MultiTreeNode> children = node.children;
                for (int j = 0;j < Constant.DEFAULT_MULTI_TREE_CHILDREN_COUNT;j++) {
                    children.add(new MultiTreeNode(randomFactory.nextInt(Constant.TREE_VALUE_RANGE)));
                }
                queue.addAll(children);
            }
            depth++;
        }
        return new MultiTree(root);
    }

    /**
     * 创建字典树
     */
    public static TrieTree createTrieTree() {
        String[] defaultDict = Constant.DEFAULT_STRING_DICT;
        TrieTree trieTree = new TrieTree(new TrieTreeNode());
        for (String s: defaultDict) {
            trieTree.add(s);
        }
        return trieTree;
    }

    /**
     * 创建随机数组
     */
    public static int[] createArray(int length) {
        int[] array = new int[length];
        for (int i = 0;i < length;i++) {
            array[i] = randomFactory.nextInt(Constant.ARRAY_LENGTH_RANGE);
        }
        return array;
    }

    /**
     * 创建并查集,以LeetCode.990(等式方程的可满足性)为例：
     * 给定一个由表示变量之间关系的字符串方程组成的数组，每个字符串方程 equations[i] 的长度为 4
     * 并采用两种不同的形式之一："a==b" 或"a!=b"。在这里，a 和 b 是小写字母（不一定不同），表示单字母变量名。
     * 只有当可以将整数分配给变量名，以便满足所有给定的方程时才返回true，否则返回 false。
     *
     * 示例：
     * 输入：["a==b","b!=a"]
     * 输出：false
     * 解释：如果我们指定，a = 1 且 b = 1，那么可以满足第一个方程，但无法满足第二个方程。没有办法分配变量同时满足这两个方程。
     *
     * 注意：
     * 1. 等式均由小写字母、等号及不等号组成
     * 2. 等式和不等式的区分点在equation[1]的位置
     */
    public static UnionFindSet createUnionFindSet(String[] equations) {
        UnionFindSet unionFindSet = new UnionFindSet(Constant.DEFAULT_UNION_FIND_SET_LENGTH);
        for (String equation: equations) {
            if (equation.charAt(1) == '=') {
                int p = equation.charAt(0) - 'a';
                int q = equation.charAt(3) - 'a';
                unionFindSet.union(p, q);
            }
        }
        return unionFindSet;
    }

    public static class Knapsack {
        public int[] weights;

        public int[] values;

        public int capacity;

        public Knapsack(int[] weights, int[] values, int capacity) {
            this.weights = weights;
            this.values = values;
            this.capacity = capacity;
        }
    }

    /**
     * 生成背包及物品
     */
    public static Knapsack createKnapsack() {
        int[] weights = new int[Constant.GOODS_COUNT];
        int[] values = new int[Constant.GOODS_COUNT];
        int capacity = 0;

        for (int i = 0;i < Constant.GOODS_COUNT;i++) {
            weights[i] = randomFactory.nextInt(Constant.ARRAY_LENGTH_RANGE);
            values[i] = randomFactory.nextInt(Constant.ARRAY_LENGTH_RANGE / 2);
            capacity += weights[i] / 3;
        }

        return new Knapsack(weights, values, capacity);
    }

    /**
     * 数组元素交换
     */
    public static void swap(int[] nums, int idx1, int idx2) {
        int tmp = nums[idx1];
        nums[idx1] = nums[idx2];
        nums[idx2] = tmp;
    }

    /**
     * 打印数组
     */
    public static void printArray(int[] nums) {
        System.out.print("[");
        for (int i = 0;i < nums.length;i++) {
            if (i < nums.length - 1) {
                System.out.print(nums[i]+", ");
            }else {
                System.out.print(nums[i]);
            }
        }
        System.out.print("]");
    }
}