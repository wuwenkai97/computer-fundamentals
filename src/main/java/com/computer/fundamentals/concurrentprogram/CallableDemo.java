package com.computer.fundamentals.concurrentprogram;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 使用Callable接口有以下五步
 * 1. 创建MyCallable类实现Callable接口
 * 2. 初始化FutureTask类，利用Callable实例作为参数
 * 3. 初始化Thread类，利用FutureTask实例作为参数
 * 4. 启动线程
 * 5. 调用futureTask实例接收参数
 */
public class CallableDemo {

    public static void main(String[] args) {
        FutureTask<String> futureTask = new FutureTask<>(new MyCallable());

        Thread t1 = new Thread(futureTask);
        t1.start();

        try {
            String res = futureTask.get();
            System.out.println("返回结果：" + res);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

class MyCallable implements Callable {

    @Override
    public String call() throws Exception {
        String res = "wwk";

        System.out.println(Thread.currentThread().getName() + "输出： " + res + "爱北京天安门！");

        return res;
    }
}
