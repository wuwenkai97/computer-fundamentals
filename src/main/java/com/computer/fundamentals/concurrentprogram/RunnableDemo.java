package com.computer.fundamentals.concurrentprogram;

/**
 * 这里以经典的线程安全问题——售票问题来演示如何使用Runnable接口，并利用synchronized来解决线程安全问题
 */
public class RunnableDemo {

    public static void main(String[] args) {
        MyRunnable myRunnable = new MyRunnable();
        Thread t1 = new Thread(myRunnable);
        Thread t2 = new Thread(myRunnable);
        Thread t3 = new Thread(myRunnable);

        t1.start();
        t2.start();
        t3.start();
    }

}


class MyRunnable implements Runnable {

    public int tickets = 100;

    public Object object = new Object();

    @Override
    public void run() {
        while (true) {
            synchronized (object) {
                if (tickets > 0) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    tickets--;
                    System.out.println(Thread.currentThread().getName() + ": 售票1张" + ", 还剩余" + tickets + "张");
                }else {
                    break;
                }
            }
        }
    }
}