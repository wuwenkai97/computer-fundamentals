package com.computer.fundamentals.datastructure;

import com.computer.fundamentals.datastructure.node.DoubleCircularLinkedNode;
import com.computer.util.Constant;
import com.computer.util.UniversalMethod;

public class DoubleCircularLinkedList {

    // 头部哨兵
    public DoubleCircularLinkedNode head;

    // 尾部哨兵
    public DoubleCircularLinkedNode tail;

    public int size;
    
    public DoubleCircularLinkedList(DoubleCircularLinkedNode head_, DoubleCircularLinkedNode tail_) {
        this.head = head_;
        this.tail = tail_;
        head.next = tail;
        tail.prev = head;
    }

    public void addFirst(int val) {
        DoubleCircularLinkedNode node = new DoubleCircularLinkedNode(val);
        head.next.prev = node;
        node.next = head.next;
        head.next = node;
        node.prev = head;
        tail.prev.next = head.next;
        head.next.prev = tail.prev;
        size++;
    }

    public void addLast(int val) {
        DoubleCircularLinkedNode node = new DoubleCircularLinkedNode(val);
        tail.prev.next = node;
        node.prev = tail.prev;
        tail.prev = node;
        node.next = tail;
        tail.prev.next = head.next;
        head.next.prev = tail.prev;
        size++;
    }

    public void removeFirst() {
        if (size <= 0) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_ZERO);
        }
        head.next.next.prev = head;
        head.next = head.next.next;
        tail.prev.next = head.next;
        head.next.prev = tail.prev;
        size--;
    }

    public void removeLast() {
        if (size <= 0) {
            throw new RuntimeException(Constant.LINKED_LIST_LENGTH_ZERO);
        }
        tail.prev.prev.next = tail;
        tail.prev = tail.prev.prev;
        tail.prev.next = head.next;
        head.next.prev = tail.prev;
        size--;
    }

    public void insert(int idx, int val_) {
        if (idx > size || idx < 1) {
            throw new RuntimeException(Constant.LINKED_LIST_INDEX_OUT_OF_RANGE);
        }
        if (idx == 1) {
            addFirst(val_);
            return;
        }
        if (idx == size) {
            addLast(val_);
            return;
        }
        DoubleCircularLinkedNode cur = head;
        while (idx > 1) {
            cur = cur.next;
            idx--;
        }
        DoubleCircularLinkedNode newNode = new DoubleCircularLinkedNode(val_);
        cur.next.prev = newNode;
        newNode.next = cur.next;
        cur.next = newNode;
        newNode.prev = cur;
        size++;
    }

    public void remove(int idx) {
        if (size <= 0) {
            throw new RuntimeException(Constant.LINKED_LIST_INDEX_OUT_OF_RANGE);
        }
        if (idx == 1) {
            removeFirst();
            return;
        }
        if (idx == size) {
            removeFirst();
            return;
        }
        DoubleCircularLinkedNode cur = head;
        while (idx > 1) {
            cur = cur.next;
            idx--;
        }
        cur.next.next.prev = cur;
        cur.next = cur.next.next;
        size--;
    }

    public void printLinked() {
        DoubleCircularLinkedNode cur = head.next;
        int count = 0;
        while (count < 2) {
            if (cur == tail.prev) {
                count++;
            }

            if (count < 2) {
                System.out.print(cur.val + Constant.DOUBLE_LINKED_LIST_SPLIT_SIGNAL);
            } else {
                System.out.print(cur.val);
            }
            cur = cur.next;
        }
    }

    public static void main(String[] args) {
        DoubleCircularLinkedList doubleCircularLinkedList = UniversalMethod.createDoubleCircularLinkedList(Constant.DEFAULT_LINKED_LIST_LENGTH);

        System.out.println("------------------完整链表------------------");
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------头插法-------------------");
        doubleCircularLinkedList.addFirst(-1);
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------尾插法-------------------");
        doubleCircularLinkedList.addLast(-1);
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------删除头部节点-------------------");
        doubleCircularLinkedList.removeFirst();
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------删除尾部节点-------------------");
        doubleCircularLinkedList.removeLast();
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------向链表某一个位置插入节点-------------------");
        doubleCircularLinkedList.insert(5, 100);
        doubleCircularLinkedList.printLinked();
        System.out.println("\n");

        System.out.println("-------------------删除某一个位置的节点-------------------");
        doubleCircularLinkedList.remove(5);
        doubleCircularLinkedList.printLinked();
    }
}