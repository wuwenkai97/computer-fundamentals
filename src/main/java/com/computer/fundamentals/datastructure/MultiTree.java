package com.computer.fundamentals.datastructure;

import com.computer.fundamentals.datastructure.node.MultiTreeNode;
import com.computer.util.UniversalMethod;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MultiTree {

    public MultiTreeNode root;

    public MultiTree(MultiTreeNode root) {
        this.root = root;
    }

    /**
     * 广度优先搜索
     */
    public List<List<Integer>> levelOrder() {
        List<List<Integer>> nums = new ArrayList<>();
        Queue<MultiTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int n = queue.size();
            List<Integer> tmp = new ArrayList<>();
            for (int i = 0;i < n;i++) {
                MultiTreeNode node = queue.poll();
                tmp.add(node.val);
                if (node.children != null) {
                    queue.addAll(node.children);
                }
            }
            nums.add(tmp);
        }
        return nums;
    }

    /**
     * 打印多叉树
     */
    public void printBinaryTreeNode() {
        List<List<Integer>> nums = levelOrder();
        for (List<Integer> num : nums) {
            System.out.println(num.toString());
        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        MultiTree multiTree = UniversalMethod.createMultiTree();

        System.out.println("----------------层序遍历----------------");
        multiTree.printBinaryTreeNode();

    }
}