package com.computer.fundamentals.datastructure.node;

public class DoubleCircularLinkedNode {
    public int val;

    public DoubleCircularLinkedNode prev;

    public DoubleCircularLinkedNode next;

    public DoubleCircularLinkedNode(int val) {
        this.val = val;
    }
}
