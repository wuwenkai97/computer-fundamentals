package com.computer.fundamentals.datastructure.node;


public class SingleCircularLinkedNode {

    public int val;

    public SingleCircularLinkedNode next;

    public SingleCircularLinkedNode(int val_) {
        this.val = val_;
    }

}
