package com.computer.fundamentals.datastructure.node;

public class SingleLinkedNode {
    public int val;
    public SingleLinkedNode next;

    public SingleLinkedNode(int val_) {
        this.val = val_;
    }

    public SingleLinkedNode(int val_, SingleLinkedNode next_) {
        this.val = val_;
        this.next = next_;
    }
}