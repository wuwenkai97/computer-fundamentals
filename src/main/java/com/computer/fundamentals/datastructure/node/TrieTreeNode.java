package com.computer.fundamentals.datastructure.node;


import com.computer.util.Constant;


public class TrieTreeNode {

    public int[] count;

    public TrieTreeNode[] children;

    public boolean isEnd;

    public TrieTreeNode() {
        // 此处指设置128个ASCII码
        this.children = new TrieTreeNode[Constant.DEFAULT_TRIE_TREE_NODE_SIZE];
        this.isEnd = false;
        this.count = new int[Constant.DEFAULT_TRIE_TREE_NODE_SIZE];
    }

}
