package com.computer.fundamentals.datastructure.node;


public class DoubleLinkedNode {

    public int val;

    public DoubleLinkedNode prev;

    public DoubleLinkedNode next;

    public DoubleLinkedNode(int val_) {
        this.val = val_;
    }

}
