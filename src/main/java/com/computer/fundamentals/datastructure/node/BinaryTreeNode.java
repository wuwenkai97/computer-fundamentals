package com.computer.fundamentals.datastructure.node;

public class BinaryTreeNode {
    public int val;

    public BinaryTreeNode left;

    public BinaryTreeNode right;

    public BinaryTreeNode(int val) {
        this.val = val;
    }
}