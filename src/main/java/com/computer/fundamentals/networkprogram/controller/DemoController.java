package com.computer.fundamentals.networkprogram.controller;

import com.computer.fundamentals.networkprogram.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("demo/")
public class DemoController {

    final UserService userService;

    public DemoController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 模拟加法，并直接打印在浏览器上
     */
    @RequestMapping("/add")
    @ResponseBody
    public int add(
            @RequestParam("a") int a,
            @RequestParam("b") int b
    ) {
        return a+b;
    }

    /**
     * 模拟 AOP
     */
    @RequestMapping("/aop")
    @ResponseBody
    public void aopTest() {
        userService.save();
    }
}