package com.computer.fundamentals.networkprogram;

import org.junit.Test;

import java.io.*;

/**
 * 字符流：传输过程中，传输数据的最基本单位是字符的流
 */
public class CharacterStream {
    /**
     * 文件读操作（逐个读取）
     */
    @Test
    public void fileReaderSlow() {
        FileReader fileReader = null;

        try {

            // 1. 实例化File类的对象，指明要作的文件
            File file = new File("IOFile\\Character.txt");
            // 2. 提供具体的流
            fileReader = new FileReader(file);
            // 3. 数据的读入
            int data;
            while ((data = fileReader.read()) != -1) {
                System.out.print((char) data);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 文件读取
     */
    @Test
    public void fileReaderFast() {
        FileReader fileReader = null;

        try {

            // 1. 实例化File类的对象，指明要作的文件
            File file = new File("IOFile\\Character.txt");
            // 2. 提供具体的流
            fileReader = new FileReader(file);
            // 3. 数据的读入
            char[] buffer = new char[5];
            int len;
            while ((len = fileReader.read(buffer)) != -1) {
                String str = new String(buffer, 0, len);
                System.out.print(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 文件写入
     */
    @Test
    public void fileWriter() {
        FileWriter fileWriter = null;

        try {
            // 1. 指明要写入的文件
            File file = new File("IOFile\\Character.txt");
            // 2. 构造输出流（并设为向后扩展而不是覆盖）
            fileWriter = new FileWriter(file, true);
            // 3. 写入数据
            fileWriter.write("\nHello, World!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 文件读取并写入一个新的文件
     */
    @Test
    public void fileReaderAndWriter() {
        FileReader fileReader = null;
        FileWriter fileWriter = null;

        try {
            File fileForRead = new File("IOFile\\Character.txt");
            File fileForWrite = new File("IOFile\\CharacterWrite.txt");

            fileReader = new FileReader(fileForRead);
            fileWriter = new FileWriter(fileForWrite, true);
            char[] buffer = new char[5];
            int len;
            while ((len = fileReader.read(buffer)) != -1) {
                fileWriter.write(buffer, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}