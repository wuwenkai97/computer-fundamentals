package com.computer.fundamentals.networkprogram;

import org.junit.Test;

import java.io.*;

/**
 * 字节流：传输过程中，传输数据的最基本单位是字节的流
 */
public class ByteStream {

    /**
     * 字节流对图片进行复制操作
     */
    @Test
    public void fileInputAndOutput() {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            File fileForInput = new File("IOFile\\龙猫.jpg");
            File fileForOutput = new File("IOFile\\龙猫copy.jpg");

            fileInputStream = new FileInputStream(fileForInput);
            fileOutputStream = new FileOutputStream(fileForOutput);

            byte[] buffer = new byte[20];
            int len;
            while ((len = fileInputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}