package com.computer.fundamentals.networkprogram.service.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class UserServiceAspect {

    /**
     * 环绕切面
     */
    @Around("execution(* com.computer.fundamentals.networkprogram.service.UserService.*(..))")
    public void saveHandle(ProceedingJoinPoint proceedingJoinPoint) {
        log.info("start");

        Object result;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            log.info("server error");
        }

        log.info("end");
    }

}
