package com.computer.fundamentals.networkprogram;

import com.computer.fundamentals.networkprogram.service.MyService;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * TCP服务器端接收请求并处理业务逻辑
 */
public class TCPServer {

    public static void main(String[] args) {
        try {
            // 创建socket，并将socket绑定到12345端口
            ServerSocket serverSocket = new ServerSocket(12345);

            // 轮询，监听12345端口的请求，直到收到客户端的请求返回连接信息后再返回
            while (true) {

                // 接收请求
                Socket socket = serverSocket.accept();

                // 获取请求信息后，处理业务逻辑
                new MyService(socket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
