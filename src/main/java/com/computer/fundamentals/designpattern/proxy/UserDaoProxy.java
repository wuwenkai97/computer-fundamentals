package com.computer.fundamentals.designpattern.proxy;

public class UserDaoProxy implements IUserDao {

    public IUserDao target;

    public UserDaoProxy(IUserDao target) {
        this.target = target;
    }

    @Override
    public void save() {
        System.out.println("开始保存数据！");
        target.save(); // 目标对象方法
        System.out.println("保存数据完毕！");
    }

}
