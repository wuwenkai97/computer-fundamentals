package com.computer.fundamentals.designpattern.proxy;

import java.lang.reflect.Proxy;

public class ProxyFactory {

    public IUserDao target;

    public ProxyFactory(IUserDao target) {
        this.target = target;
    }

    /**
     * 创建代理对象
     */
    public Object getProxyInstance() {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                (o, method, objects) -> {
                    System.out.println("开始事务");
                    // 执行目标事务
                    Object returnVal = method.invoke(target, objects);
                    System.out.println("结束事务");
                    return returnVal;
                }
        );
    }
}