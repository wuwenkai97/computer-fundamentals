package com.computer.fundamentals.designpattern.proxy;

/**
 * 目标接口
 */
public interface IUserDao {
    void save();
}
