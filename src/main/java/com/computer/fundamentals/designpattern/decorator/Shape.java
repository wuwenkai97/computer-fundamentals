package com.computer.fundamentals.designpattern.decorator;

public interface Shape {
    void draw();
}
