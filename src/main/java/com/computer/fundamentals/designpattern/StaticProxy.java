package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.proxy.UserDao;
import com.computer.fundamentals.designpattern.proxy.UserDaoProxy;

/**
 * 代理(Proxy)定义：
 *      是一种设计模式,提供了对目标对象另外的访问方式;
 *      即通过代理对象访问目标对象.
 *      这样做的好处是:可以在目标对象实现的基础上,增强额外的功能操作,即扩展目标对象的功能.
 *
 * 静态代理：静态代理在使用时,需要定义接口或者父类,被代理对象与代理对象一起实现相同的接口或者是继承相同父类.
 */
public class StaticProxy {

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("------------开始测试-----------");
        UserDaoProxy userDaoProxy = new UserDaoProxy(new UserDao());
        userDaoProxy.save();

    }
}
