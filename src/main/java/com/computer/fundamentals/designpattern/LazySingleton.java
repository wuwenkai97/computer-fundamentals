package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.datastructure.SingleLinkedList;

/**
 * 单例模式：懒汉式
 *
 * 优点：懒加载，节省内存
 * 缺点：需要加锁，性能受限
 * 使用场景：Spring中的singleton
 */
public class LazySingleton {

    public static SingleLinkedList instance;

    private LazySingleton() {}
    /**
     * 获取实例
     */
    public synchronized static SingleLinkedList getInstance() {
        if (instance != null) {
            instance = new SingleLinkedList();
        }

        return instance;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println(getInstance());

    }
}
