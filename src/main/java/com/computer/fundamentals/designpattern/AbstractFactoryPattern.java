package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.factory.bean.Color;
import com.computer.fundamentals.designpattern.factory.bean.Shape;
import com.computer.fundamentals.designpattern.factory.AbstractFactory;
import com.computer.fundamentals.designpattern.factory.ColorFactory;
import com.computer.fundamentals.designpattern.factory.ShapeFactory;
import com.computer.fundamentals.designpattern.factory.factoryenum.FactoryType;
import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;
import com.computer.fundamentals.designpattern.factory.factoryenum.ShapeType;
import com.computer.util.Constant;

/**
 * 抽象工厂模式：提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类
 *
 * 使用场景：QQ 换皮肤，一整套一起换
 */
public class AbstractFactoryPattern {

    /**
     * 创建工厂
     */
    public AbstractFactory getFactory(String factoryType) {
        if (factoryType == null || factoryType.equals("")) {
            throw new RuntimeException(Constant.PARAMETER_IS_NULL);
        }

        if (factoryType.equals(FactoryType.COLOR.name())) {
            return new ColorFactory();
        }else if (factoryType.equals(FactoryType.SHAPE.name())) {
            return new ShapeFactory();
        }

        return null;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        AbstractFactoryPattern producer = new AbstractFactoryPattern();

        System.out.println("------------颜色工厂测试------------");
        AbstractFactory colorFactory = producer.getFactory(FactoryType.COLOR.name());
        System.out.println("------------红色类测试------------");
        Color red = colorFactory.getColor(RGB.RED.name());
        red.fill();
        System.out.println("------------蓝色类测试------------");
        Color blue = colorFactory.getColor(RGB.BLUE.name());
        blue.fill();
        System.out.println("------------黄色类测试测试------------");
        Color yellow = colorFactory.getColor(RGB.YELLOW.name());
        yellow.fill();
        System.out.println("\n");


        System.out.println("------------形状工厂测试------------");
        AbstractFactory shapeFactory = producer.getFactory(FactoryType.SHAPE.name());
        System.out.println("------------矩形测试------------");
        Shape rectangle = shapeFactory.getShape(ShapeType.RECTANGLE.name());
        rectangle.draw();
        System.out.println("------------正方形测试------------");
        Shape square = shapeFactory.getShape(ShapeType.Square.name());
        square.draw();
        System.out.println("------------圆形测试测试------------");
        Shape circle = shapeFactory.getShape(ShapeType.Circle.name());
        circle.draw();

    }
}