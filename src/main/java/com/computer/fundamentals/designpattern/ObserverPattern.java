package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.observer.ObserverFirst;
import com.computer.fundamentals.designpattern.observer.ObserverSecond;
import com.computer.fundamentals.designpattern.observer.Subject;

/**
 * 观察者模式：定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。
 *
 * 使用场景：
 *      在一个UGC场景下，用户发布的内容往往会经过很多流程，大部分是先发往审核系统，
 *      当审核通过之后就会出现一系列的业务逻辑，比如更新内容状态，通知给所有的粉丝等等。
 */
public class ObserverPattern {

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("------------开始测试------------");
        Subject subject = new Subject();
        System.out.println("初始状态：" + subject.getState());
        new ObserverFirst(subject);
        new ObserverSecond(subject);
        System.out.println("\n");

        System.out.println("第一次状态变更：0-15");
        subject.setState(15);
        System.out.println("\n");

        System.out.println("第二次状态变更：15-10");
        subject.setState(10);

    }
}