package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.factory.bean.Blue;
import com.computer.fundamentals.designpattern.factory.bean.Color;
import com.computer.fundamentals.designpattern.factory.bean.Yellow;
import com.computer.fundamentals.designpattern.factory.bean.Red;
import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;
import com.computer.util.Constant;

/**
 * 简单工厂模式：定义一个创建对象的接口，让其子类自己决定实例化哪一个工厂类，工厂模式使其创建过程延迟到子类进行。
 *
 * 优点：
 *      1. 一个调用者想创建一个对象，只要知道其名称就可以了
 *      2. 扩展性高，如果想增加一个产品，只要扩展一个工厂类就可以
 *      3. 屏蔽产品的具体实现，调用者只关心产品的接口
 * 缺点：
 *      1. 每次增加一个产品时，都需要增加一个具体类和对象实现工厂，使得系统中类的个数成倍增加，在一定程度上增加了系统的复杂度
 *      2. 增加了系统具体类的依赖
 *
 * 使用场景：数据库访问，当用户不知道最后系统采用哪一类数据库，以及数据库可能有变化时
 */
public class SampleFactoryPattern {

    /**
     * 制造工厂
     */
    public Color getColor(String colorType) {
        if (colorType == null || colorType.equals("")) {
            throw new RuntimeException(Constant.PARAMETER_IS_NULL);
        }

        if (colorType.equals(RGB.RED.name())) {
            return new Red();
        }else if (colorType.equals(RGB.BLUE.name())) {
            return new Blue();
        }else if (colorType.equals(RGB.YELLOW.name())) {
            return new Yellow();
        }

        return null;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        SampleFactoryPattern sampleFactoryPattern = new SampleFactoryPattern();
        Color red = sampleFactoryPattern.getColor(RGB.RED.name());
        red.fill();
        Color blue = sampleFactoryPattern.getColor(RGB.BLUE.name());
        blue.fill();
        Color yellow = sampleFactoryPattern.getColor(RGB.YELLOW.name());
        yellow.fill();

    }
}