package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.datastructure.SingleLinkedList;

/**
 * 单例模式：饿汉式
 *
 * 优点：实现简单，执行效率高（无锁），线程安全（利用类加载机制避免了多线程环境下的安全问题）
 * 缺点：类加载时就初始化，浪费内存
 */
public class EagerSingleton {

    public static SingleLinkedList instance = new SingleLinkedList();

    private EagerSingleton() {}

    /**
     * 获取实例
     */
    public static SingleLinkedList getInstance() {
        return instance;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println(getInstance());

    }
}