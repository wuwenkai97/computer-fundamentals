package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.template.Basketball;
import com.computer.fundamentals.designpattern.template.Football;

/**
 * 模板模式：
 *      1. 定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。
 *      2. 模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
 *
 * 使用场景：
 *      1. 算法的整体步骤很固定，但其中个别部分易变时，这时候可以使用模板方法模式，将容易变的部分抽象出来，供子类实现。
 *      2. 当多个子类存在公共的行为时，可以将其提取出来并集中到一个公共父类中以避免代码重复。
 *         首先，要识别现有代码中的不同之处，并且将不同之处分离为新的操作。
 *         最后，用一个调用这些新的操作的模板方法来替换这些不同的代码。
 */
public class TemplatePattern {

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("------------开始测试------------");
        System.out.println("------------篮球游戏测试------------");
        Basketball basketball = new Basketball();
        basketball.play();
        System.out.println("\n");

        System.out.println("------------足球游戏测试------------");
        Football football = new Football();
        football.play();

    }

}
