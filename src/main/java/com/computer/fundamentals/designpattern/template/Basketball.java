package com.computer.fundamentals.designpattern.template;

public class Basketball extends Game{

    @Override
    void initialize() {
        System.out.println("篮球游戏初始化!");
    }

    @Override
    void startPlay() {
        System.out.println("开始篮球游戏!");
    }

    @Override
    void endPlay() {
        System.out.println("篮球游戏结束！");
    }
}
