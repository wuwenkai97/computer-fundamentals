package com.computer.fundamentals.designpattern.observer;

public class ObserverSecond extends Observer{

    public ObserverSecond(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("Second Observer：" + subject.getState());
    }
}
