package com.computer.fundamentals.designpattern.observer;

public class ObserverFirst extends Observer {

    public ObserverFirst(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("First Observer：" + subject.getState());
    }
}
