package com.computer.fundamentals.designpattern.observer;

public abstract class Observer {

    public Subject subject;

    public abstract void update();

}
