package com.computer.fundamentals.designpattern.factory;

import com.computer.fundamentals.designpattern.factory.bean.*;
import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;
import com.computer.util.Constant;

public class ColorFactory extends AbstractFactory{

    /**
     * 制造工厂
     */
    public Color getColor(String colorType) {
        if (colorType == null || colorType.equals("")) {
            throw new RuntimeException(Constant.PARAMETER_IS_NULL);
        }

        if (colorType.equals(RGB.RED.name())) {
            return new Red();
        }else if (colorType.equals(RGB.BLUE.name())) {
            return new Blue();
        }else if (colorType.equals(RGB.YELLOW.name())) {
            return new Yellow();
        }

        return null;
    }

    @Override
    public Shape getShape(String shape) {
        return null;
    }
}
