package com.computer.fundamentals.designpattern.factory.bean;

import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;

public class Blue implements Color {
    @Override
    public void fill() {
        System.out.println(RGB.BLUE.name());
    }
}
