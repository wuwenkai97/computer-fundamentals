package com.computer.fundamentals.designpattern.factory.bean;

import com.computer.fundamentals.designpattern.factory.factoryenum.ShapeType;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println(ShapeType.Circle.name());
    }
}
