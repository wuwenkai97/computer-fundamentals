package com.computer.fundamentals.designpattern.factory.bean;

import com.computer.fundamentals.designpattern.factory.factoryenum.ShapeType;

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println(ShapeType.RECTANGLE.name());
    }
}
