package com.computer.fundamentals.designpattern.factory.factoryenum;

public enum FactoryType {
    COLOR,SHAPE
}
