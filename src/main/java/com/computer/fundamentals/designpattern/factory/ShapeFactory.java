package com.computer.fundamentals.designpattern.factory;

import com.computer.fundamentals.designpattern.factory.bean.*;
import com.computer.fundamentals.designpattern.factory.factoryenum.ShapeType;
import com.computer.util.Constant;

public class ShapeFactory extends AbstractFactory{
    @Override
    public Color getColor(String color) {
        return null;
    }

    /**
     * 制造工厂
     */
    @Override
    public Shape getShape(String shapeType) {
        if (shapeType == null || shapeType.equals("")) {
            throw new RuntimeException(Constant.PARAMETER_IS_NULL);
        }

        if (shapeType.equals(ShapeType.RECTANGLE.name())) {
            return new Rectangle();
        }else if (shapeType.equals(ShapeType.Square.name())) {
            return new Square();
        }else if (shapeType.equals(ShapeType.Circle.name())) {
            return new Circle();
        }

        return null;
    }
}
