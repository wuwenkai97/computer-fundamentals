package com.computer.fundamentals.designpattern.factory.bean;

import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;

public class Red implements Color {
    @Override
    public void fill() {
        System.out.println(RGB.RED.name());
    }
}
