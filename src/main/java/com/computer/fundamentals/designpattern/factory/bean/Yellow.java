package com.computer.fundamentals.designpattern.factory.bean;

import com.computer.fundamentals.designpattern.factory.factoryenum.RGB;

public class Yellow implements Color {
    @Override
    public void fill() {
        System.out.println(RGB.YELLOW.name());
    }
}
