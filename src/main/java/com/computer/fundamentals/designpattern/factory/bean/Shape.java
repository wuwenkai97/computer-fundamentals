package com.computer.fundamentals.designpattern.factory.bean;

public interface Shape {
    void draw();
}
