package com.computer.fundamentals.designpattern.factory.bean;

public interface Color {
    void fill();
}
