package com.computer.fundamentals.designpattern.factory;

import com.computer.fundamentals.designpattern.factory.bean.Color;
import com.computer.fundamentals.designpattern.factory.bean.Shape;

public abstract class AbstractFactory {
    public abstract Color getColor(String color);

    public abstract Shape getShape(String shape);
}
