package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.decorator.Circle;
import com.computer.fundamentals.designpattern.decorator.Rectangle;
import com.computer.fundamentals.designpattern.decorator.RedShapeDecorator;

/**
 * 装饰器模式：动态地给一个对象添加一些额外的职责。就增加功能来说，装饰器模式相比生成子类更为灵活。
 *
 * 使用场景：替代继承
 */
public class DecoratorPattern {

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("------------开始测试------------");
        System.out.println("------------原始功能------------");
        Circle circle = new Circle();
        circle.draw();
        System.out.println("\n");

        System.out.println("------------圆形染成红色------------");
        RedShapeDecorator redShapeDecoratorCircle = new RedShapeDecorator(new Circle());
        redShapeDecoratorCircle.draw();
        System.out.println("\n");

        System.out.println("------------原始功能------------");
        Rectangle rectangle = new Rectangle();
        rectangle.draw();
        System.out.println("\n");

        System.out.println("------------矩形染成红色------------");
        RedShapeDecorator redShapeDecoratorRectangle = new RedShapeDecorator(new Rectangle());
        redShapeDecoratorRectangle.draw();
    }
}