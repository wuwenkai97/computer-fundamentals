package com.computer.fundamentals.designpattern;

import com.computer.fundamentals.designpattern.proxy.IUserDao;
import com.computer.fundamentals.designpattern.proxy.ProxyFactory;
import com.computer.fundamentals.designpattern.proxy.UserDao;

/**
 * 代理(Proxy)定义：
 *      是一种设计模式,提供了对目标对象另外的访问方式;
 *      即通过代理对象访问目标对象.
 *      这样做的好处是:可以在目标对象实现的基础上,增强额外的功能操作,即扩展目标对象的功能.
 *
 * 动态代理：代理对象,不需要实现接口
 */
public class DynamicProxy {

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("------------开始测试-----------");
        // 根据目标对象创建代理工厂
        ProxyFactory proxyFactory = new ProxyFactory(new UserDao());
        // 创建代理对象
        IUserDao proxyInstance = (IUserDao) proxyFactory.getProxyInstance();
        // 在内存中生成代理对象
        System.out.println(proxyInstance.getClass());
        // 执行代理对象方法
        proxyInstance.save();

    }
}