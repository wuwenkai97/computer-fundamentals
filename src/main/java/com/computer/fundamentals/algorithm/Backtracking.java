package com.computer.fundamentals.algorithm;

import com.computer.util.UniversalMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * 回溯算法
 * 回溯算法也叫试探法，它是一种系统地搜索问题的解的方法。
 * 用回溯算法解决问题的一般步骤：
 * 1. 针对所给问题，定义问题的解空间，它至少包含问题的一个（最优）解。
 * 2. 确定易于搜索的解空间结构,使得能用回溯法方便地搜索整个解空间 。
 * 3. 以深度优先的方式搜索解空间，并且在搜索过程中用剪枝函数避免无效搜索。
 */
public class Backtracking {

    /**
     * 以全排列问题(不包含重复排列)为例
     */
    public List<List<Integer>> wholeArrangement(int[] nums) {
        Arrays.sort(nums);
        int[] memory = new int[nums.length];
        List<List<Integer>> res = new ArrayList<>();
        recall(nums, memory, new Stack<>(), res);
        return res;
    }

    /**
     * 回溯
     */
    private void recall(int[] nums, int[] memory, Stack<Integer> tmp, List<List<Integer>> res) {
        if (tmp.size() == nums.length) {
            res.add(new ArrayList<>(tmp));
            return;
        }
        for (int i = 0;i < nums.length;i++) {
            if (i > 0 && nums[i-1] == nums[i] && memory[i-1] == 0) {
                continue;
            }
            if (memory[i] == 1) {
                continue;
            }
            memory[i] = 1;
            tmp.push(nums[i]);
            recall(nums, memory, tmp, res);
            memory[i] = 0;
            tmp.pop();
        }
    }

    /**
     * 打印排列结果
     */
    public void printArrangement(List<List<Integer>> res) {
        for (List<Integer> list : res) {
            System.out.println(list.toString());
        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        Backtracking backtracking = new Backtracking();
        int[] array = UniversalMethod.createArray(4);

        System.out.println("------------原始数组------------");
        UniversalMethod.printArray(array);
        System.out.println("\n");

        System.out.println("------------回溯测试------------");
        List<List<Integer>> res = backtracking.wholeArrangement(array);
        backtracking.printArrangement(res);
    }
}