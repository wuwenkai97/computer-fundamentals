package com.computer.fundamentals.algorithm.sort;

import com.computer.util.Constant;
import com.computer.util.UniversalMethod;

public class BubbleSort {

    /**
     * 冒泡排序
     */
    public static int[] sort(int[] nums) {
        int j = nums.length-1;
        while (j > 0) {
            int i = 0;
            boolean flag = true;
            for (;i < j;i++) {
                if (nums[i] > nums[i+1]) {
                    int tmp = nums[i];
                    nums[i] = nums[i+1];
                    nums[i+1] = tmp;
                    flag = false;
                }
            }
            if (flag) {
                break;
            }
            j--;
        }
        return nums;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("---------------原数组---------------");
        int[] array = UniversalMethod.createArray(Constant.DEFAULT_ARRAY_LENGTH);
        UniversalMethod.printArray(array);
        System.out.println("\n");

        System.out.println("---------------排序数组---------------");
        sort(array);
        UniversalMethod.printArray(array);

    }
}
