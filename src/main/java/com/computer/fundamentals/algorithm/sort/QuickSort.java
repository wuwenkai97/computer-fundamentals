package com.computer.fundamentals.algorithm.sort;

import com.computer.util.Constant;
import com.computer.util.UniversalMethod;

public class QuickSort {

    /**
     * 快速排序
     */
    public static void quickSort(int[] nums, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = left;
        int i = left, j = right;
        while (i < j) {
            while (i < j && nums[j] > nums[pivot]) {
                j--;
            }
            while (i < j && nums[i] <= nums[pivot]) {
                i++;
            }
            UniversalMethod.swap(nums, i, j);
        }
        UniversalMethod.swap(nums, j, pivot);
        quickSort(nums, left, j-1);
        quickSort(nums, j+1, right);
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("---------------原数组---------------");
        int[] array = UniversalMethod.createArray(Constant.DEFAULT_ARRAY_LENGTH);
        UniversalMethod.printArray(array);
        System.out.println("\n");

        System.out.println("---------------排序数组---------------");
        quickSort(array, 0, array.length-1);
        UniversalMethod.printArray(array);
    }
}