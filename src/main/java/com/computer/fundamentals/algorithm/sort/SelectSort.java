package com.computer.fundamentals.algorithm.sort;

import com.computer.util.Constant;
import com.computer.util.UniversalMethod;

public class SelectSort {

    /**
     * 选择排序
     */
    public static int[] selectSort(int[] nums) {
        for (int i = nums.length-1;i >= 0;i--) {
            int maxValue = nums[i];
            int maxIdx = i;
            for (int j = 0;j <= i;j++) {
                if (nums[j] > maxValue) {
                    maxValue = nums[j];
                    maxIdx = j;
                }
            }
            UniversalMethod.swap(nums, maxIdx, i);
        }

        return nums;
    }


    /**
     * 测试
     */
    public static void main(String[] args) {
        System.out.println("---------------原数组---------------");
        int[] array = UniversalMethod.createArray(Constant.DEFAULT_ARRAY_LENGTH);
        UniversalMethod.printArray(array);
        System.out.println("\n");

        System.out.println("---------------排序数组---------------");
        selectSort(array);
        UniversalMethod.printArray(array);
    }

}
