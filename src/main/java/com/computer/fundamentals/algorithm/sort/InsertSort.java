package com.computer.fundamentals.algorithm.sort;

import com.computer.util.Constant;
import com.computer.util.UniversalMethod;

public class InsertSort {

    /**
     * 插入排序
     */
    public static void insertSort(int[] nums) {
        for (int i = 1;i < nums.length;i++) {
            int j = i-1;
            while (j >= 0 && nums[j] > nums[j+1]) {
                UniversalMethod.swap(nums, j, j+1);
                j--;
            }
        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {

        System.out.println("---------------原数组---------------");
        int[] array = UniversalMethod.createArray(Constant.DEFAULT_ARRAY_LENGTH);
        UniversalMethod.printArray(array);
        System.out.println("\n");

        System.out.println("---------------排序数组---------------");
        insertSort(array);
        UniversalMethod.printArray(array);

    }
}